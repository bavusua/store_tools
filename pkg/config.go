package pkg

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func InitConfig() {
	// Set the file name of the configurations file
	viper.SetConfigName("conf")

	// Set the path to look for the configurations file
	viper.AddConfigPath("/etc/systemd/")
	//viper.AddConfigPath(".")

	// Enable VIPER to read Environment Variables
	viper.AutomaticEnv()

	viper.SetConfigType("toml")

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}
	logrus.Info("Initiated configuration")
}
