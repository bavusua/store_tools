package pkg

import (
	"bitbucket.org/bavusua/store_tools/api/models"
	order_model "bitbucket.org/bavusua/store_tools/api/models/order"
	product_model "bitbucket.org/bavusua/store_tools/api/models/product"
	user_model "bitbucket.org/bavusua/store_tools/api/models/user"
	ghnService "bitbucket.org/bavusua/store_tools/api/services/ghn"
	"bitbucket.org/bavusua/store_tools/api/utils"
	"context"
	"github.com/spf13/viper"
	"time"
)

type OrderPackageCommon struct {
	Manager *order_model.OrderManager
	Log     string
}

func (h *OrderPackageCommon) CreateOrder(o *order_model.OrderDto, user *user_model.User, product *product_model.Product) error {
	var publishedCost float64
	if product.ShippingOption.HasCostOnPublish {
		publishedCost = product.ShippingOption.PublishCost
	}
	o.UserId = user.Id
	o.FirstUserId = user.Id
	o.ProductId = product.Id
	o.Status = utils.STATUS_CREATED
	o.CreatedAt = o.PlacedAt
	o.UpdatedAt = o.PlacedAt
	o.LineItems = []*order_model.LineItem{
		{
			ProductId:   product.Id,
			ProductName: product.Name,
			Quantity:    product.QuantityInPack,
			Price:       product.Cost,
			PublishCost: publishedCost,
		},
	}
	o.Logs = []*models.HistoryLog{
		{
			UserName: user.Username,
			Action:   h.Log,
			Time:     time.Now().Unix(),
		},
	}
	o.GHNRequest = InitGHNRequest(o.CustomerName, o.PhoneNumber, product)
	err := h.Manager.CreateDto(context.Background(), o)
	return err
}

func InitGHNRequest(customerName, phoneName string, product *product_model.Product) *ghnService.GHNOrder {
	return &ghnService.GHNOrder{
		CustomerName:       customerName,
		CustomerPhone:      phoneName,
		Weight:             product.ShippingOption.Weight,
		Length:             product.ShippingOption.Length,
		Width:              product.ShippingOption.Width,
		Height:             product.ShippingOption.Height,
		FromDistrictID:     viper.GetInt64("ghn_service.DistrictID"),
		FromWardCode:       viper.GetString("ghn_service.WardCode"),
		ClientContactName:  viper.GetString("ghn_service.ContactName"),
		ClientContactPhone: viper.GetString("ghn_service.ContactPhone"),
		ClientAddress:      viper.GetString("ghn_service.Address"),
		ReturnDistrictID:   viper.GetInt64("ghn_service.DistrictID"),
		ReturnContactName:  viper.GetString("ghn_service.ContactName"),
		ReturnContactPhone: viper.GetString("ghn_service.ContactPhone"),
		ReturnAddress:      viper.GetString("ghn_service.Address"),
		ExternalReturnCode: viper.GetString("ghn_service.ExternalReturnCode"),
		AffiliateID:        viper.GetInt64("ghn_service.AffiliateID"),
		ServiceName:        product.ShippingOption.ShippingOption,
		NoteCode:           product.ShippingOption.OrderRequirement,
	}
}

func TransformGHNBaseRequest(orderDto *order_model.OrderDto, product *product_model.Product) {
	orderDto.GHNRequest.CustomerName = orderDto.CustomerName
	orderDto.GHNRequest.CustomerPhone = orderDto.PhoneNumber
	orderDto.GHNRequest.Weight = product.ShippingOption.Weight
	orderDto.GHNRequest.Length = product.ShippingOption.Length
	orderDto.GHNRequest.Width = product.ShippingOption.Width
	orderDto.GHNRequest.Height = product.ShippingOption.Height
	orderDto.GHNRequest.FromDistrictID = viper.GetInt64("ghn_service.DistrictID")
	orderDto.GHNRequest.FromWardCode = viper.GetString("ghn_service.WardCode")
	orderDto.GHNRequest.ClientContactName = viper.GetString("ghn_service.ContactName")
	orderDto.GHNRequest.ClientContactPhone = viper.GetString("ghn_service.ContactPhone")
	orderDto.GHNRequest.ClientAddress = viper.GetString("ghn_service.Address")
	orderDto.GHNRequest.ReturnDistrictID = viper.GetInt64("ghn_service.DistrictID")
	orderDto.GHNRequest.ReturnContactName = viper.GetString("ghn_service.ContactName")
	orderDto.GHNRequest.ReturnContactPhone = viper.GetString("ghn_service.ContactPhone")
	orderDto.GHNRequest.ReturnAddress = viper.GetString("ghn_service.Address")
	orderDto.GHNRequest.ExternalReturnCode = viper.GetString("ghn_service.ExternalReturnCode")
	orderDto.GHNRequest.AffiliateID = viper.GetInt64("ghn_service.AffiliateID")
	orderDto.GHNRequest.ServiceName = product.ShippingOption.ShippingOption
	orderDto.GHNRequest.NoteCode = product.ShippingOption.OrderRequirement
}

func GetNextAssignee(telesales []*user_model.User, lastOrder *order_model.Order) *user_model.User {
	if lastOrder == nil {
		return telesales[0]
	}
	for i, u := range telesales {
		if u.Id == lastOrder.FirstUserId {
			if i+1 == len(telesales) {
				return telesales[0]
			} else {
				return telesales[i+1]
			}
		}
	}
	return telesales[0]
}
