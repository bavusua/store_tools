package loggers

import (
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	_log      *log.Logger
)

func init() {
	// set location of log file
	getLog()
}

func getLog() *log.Logger {
	defer func() {
		if rerr := recover(); rerr != nil {
			fmt.Println("ERROR INIT LOG: ", rerr)
		}
	}()
	if _log != nil  {
		return _log
	}
	var logpath = "/etc/logger/storetools.log"

	flag.Parse()
	file, err := os.Create(logpath)
	if err != nil {
		//panic(err)
		return nil
	}
	//defer file.Close()

	_log = log.New(file, "", log.LstdFlags|log.Lshortfile)
	_log.Println("LogFile : " + logpath)
	return _log
}

func Infow(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Info, message: %v }", mess), args)
}

func Errorw(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Error, message: %v }", mess), args)
}

func Warnw(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Warn, message: %v }", mess), args)
}

