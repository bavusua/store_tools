package main

import (
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"bitbucket.org/bavusua/store_tools/scheduler/gg_services"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"store_tools/pkg"
	"time"
)

func main() {
	pkg.InitConfig()
	d := time.Second * time.Duration(viper.GetInt("ggs_service.scan_ticker"))
	if d == 0 {
		d = time.Minute * 5
	}
	ticker := time.NewTicker(d)
	logrus.Info("Initiated scanner")
	//myChannel(chan , 1)
	success := true
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			if !success {
				break
			}
			success = false
			go func() {
				success = startWorking()
			}()
		}
	}
}

func startWorking() (endProcess bool) {
	defer func() {
		if re := recover(); re != nil {
			//log re, push slack?
		}
		endProcess = true
	}()
	db, err := sqlServive.InitDBConnection()
	if err != nil {
		panic(err)
	}
	defer db.Close()
	handler := gg_services.Init(db)
	handler.Process()
	return true
}
