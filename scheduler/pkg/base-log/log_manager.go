package log

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

var (
	_log      *log.Logger
)

func init() {
	// set location of log file
	getLog()
}

func getLog() *log.Logger {
	if _log != nil  {
		return _log
	}
	var (
		name = "/etc/logger/info"
		ext = "log"
		logpath = fmt.Sprintf("%v.%v", name, ext)
	)

	flag.Parse()

	file, err := os.Open(logpath)
	if err == nil {
		func () {
			defer file.Close()
			data, err := ioutil.ReadAll(file)
			if err == nil {
				desSource := fmt.Sprintf("%v_%v.%v", name, time.Now().Unix(), ext)
				_ = ioutil.WriteFile(desSource, data, 0666)
			}
		}()
	}


	file, err = os.Create(logpath)
	if err != nil {
		//panic(err)
		return nil
	}

	_log = log.New(file, "", log.LstdFlags|log.Lshortfile)
	_log.Println("LogFile : " + logpath)
	return _log
}

func Infow(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Info, message: %v }", mess), args)
}

func Errorw(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Error, message: %v }", mess), args)
}

func Warnw(mess string,args ...interface{}) {
	localLog := getLog()
	if localLog == nil {
		return
	}
	localLog.Println(fmt.Sprintf("{ level: Warn, message: %v }", mess), args)
}
