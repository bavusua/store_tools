package independent_utils

import (
	"bitbucket.org/bavusua/store_tools/api/models"
	order_model "bitbucket.org/bavusua/store_tools/api/models/order"
	"context"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/cast"
	"golang.org/x/oauth2"
	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	RequestTokenInternalURL = "http://localhost:1111/get-auth-code"
	SendTokenInternalURL = "http://localhost:1111/set-auth-url"
	ResetAuthCodeInternalURL = "http://localhost:1111/set-auth-code"
	ClearCacheProducts	 	= "http://localhost:1111/products/cache"
	TokenFilePath = "/etc/systemd/token.json"
	//TokenFilePath = "token.json"
)

type ResponseTokenStruct struct {
	Result struct{
		AuthCode string `json:"auth_code"`
	} `json:"result"`
}

// Retrieves a token from a local file.
func test_tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Request a token from the web, then returns the retrieved token.
func test_getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

func GetTestClient(config *oauth2.Config) *http.Client {
	tokFile := "token.json"
	tok, err := test_tokenFromFile(tokFile)
	if err != nil {
		tok = test_getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

func GetClient(config *oauth2.Config, authCode string) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tok, err := tokenFromFile()
	if err != nil {
		if authCode == "" {
			res, err := getAuthCodeByInternalApi()
			if err != nil {
				return nil
			}
			if res != "" {
				authCode = res
			} else {
				authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
				sendRequestToGetToken(authURL)
				return nil
			}
		}
		tok = getTokenFromWeb(config, authCode)
		if tok == nil {
			return nil
		}
		saveToken(TokenFilePath, tok)
	}
	return config.Client(context.Background(), tok)
}

func sendRequestToGetToken(authURL string)  {
	client := &http.Client{}
	val := &struct {
		AuthURL string `json:"auth_url"`
	}{
		AuthURL: authURL,
	}
	jsonData, _ := json.Marshal(val)
	if rs, err := client.Post( SendTokenInternalURL, "json", strings.NewReader(string(jsonData))); err != nil {
		fmt.Printf("Error post message auth url: %v \n", err.Error())
	} else {
		if rs.StatusCode != 200 {
			fmt.Println("got error sendRequestToGetToken", rs)
		}
	}
}

func sendRequestClearAuthCode() {
	client := &http.Client{}
	val := &struct {
		AuthCode string `json:"auth_code"`
	}{
		AuthCode: "",
	}
	jsonData, _ := json.Marshal(val)
	if _, err := client.Post(ResetAuthCodeInternalURL, "json", strings.NewReader(string(jsonData))); err != nil {
		fmt.Printf("Error reset message auth authCode: %v \n", err.Error())
	}
}

func SendRequestClearCacheProducts() {
	client := &http.Client{}
	client.Post(ClearCacheProducts, "text/plain", nil)
}

func getAuthCodeByInternalApi() (string, error) {
	client := &http.Client{}
	resp, err := client.Get(RequestTokenInternalURL)
	if err != nil {
		return "", err
	}
	if resp.StatusCode != 200 {
		return "", fmt.Errorf("error get response AuthCode, res: %v", resp)
	}
	defer func() {
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}
	}()
	tok := ResponseTokenStruct{}
	err = json.NewDecoder(resp.Body).Decode(&tok)
	if err != nil {
		return "", err
	}
	authCode := strings.Trim(tok.Result.AuthCode, " ")

	if authCode != "" {
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			defer wg.Done()
			sendRequestClearAuthCode()
		}()
		wg.Add(1)
		go func() {
			defer wg.Done()
			sendRequestToGetToken("")
		}()
		wg.Wait()
	}

	return authCode, nil
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config, authCode string) *oauth2.Token {

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		fmt.Printf("Unable to retrieve token from web: %v", err)
		return nil
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile() (*oauth2.Token, error) {
	f, err := os.Open(TokenFilePath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		fmt.Println(fmt.Sprintf("Unable to cache oauth token: %v", err))
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func GetNewOrdersInProductSheet(ggs *models.GGSheetDetails) (orders []*order_model.Order, err error) {
	var (
		srv *sheets.Service
		resp *sheets.ValueRange
	)
	srv, err = getGGSService()
	go func() {
		defer func() {
			if rerr := recover(); rerr != nil {
				fmt.Println("Error while try delete token json file", rerr)
			}
		}()
		if err != nil {
			os.Remove(TokenFilePath)
		}
	}()
	if err != nil {
		//fmt.Print(err)
		return
	}
	readRange := fmt.Sprintf("%v!A%v:K", ggs.Name, ggs.LastRowScan+1)
	resp, err = srv.Spreadsheets.Values.Get(ggs.Id, readRange).Do()
	if err != nil {
		fmt.Print(err)
		return
	}
	orders = make([]*order_model.Order, 0)
	for _, row := range resp.Values {
		orders = append(orders, convertRowValues(row))
	}
	return
}

func getGGSService() (*sheets.Service, error) {
	config, err := GetConfigHardCode()
	if err != nil {
		fmt.Printf("unable to parse client secret file to config , error: %v", err)
		return nil, err
	}

	client := GetClient(config, "")
	//client := GetTestClient(config)
	if client == nil {
		return nil, fmt.Errorf("error get token google sheet")
	}
	return sheets.NewService(context.Background(), option.WithHTTPClient(client))
}
func convertRowValues(row []interface{}) *order_model.Order {
	order := &order_model.Order{}
	for idxCol, _ := range row {
		val := cast.ToString(row[idxCol])
		switch idxCol {
		case 0:
			if strings.TrimSpace(val) != "" {
				val = strings.Replace(val, " ", "T", 1)
				val += "Z"
				t, err := time.Parse(time.RFC3339, val)
				if err != nil {
					fmt.Println(err)
					t = time.Now()
				}
				order.PlacedAt = t.Unix()
			}
		case 1:
			order.CustomerName = val
		case 2:
			if len(val) == 9 {
				val = "0" + val
			}
			order.PhoneNumber = val
		case 3:
			order.InputAddress = val
		case 4:
			order.LandingPage = val
		case 5:
			order.Source = val
		case 10:
			order.CustomerIP = val
		}
	}
	return order
}
