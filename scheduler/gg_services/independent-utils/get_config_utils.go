package independent_utils

import (
	"errors"
	"fmt"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
)

const (
	//ClientID	 = "810274551410-aksop1g36lb6hsi9d37nl139du2bplpo.apps.googleusercontent.com"
	//ClientSecret = "rK71Badqxa7J4MALiwxRrqD7"
	Scope        = "https://www.googleapis.com/auth/spreadsheets.readonly"
	AuthURI      = "https://accounts.google.com/o/oauth2/auth"
	TokenURI     = "https://oauth2.googleapis.com/token"
)

func GetConfigHardCode() (*oauth2.Config, error) {
	clientId := viper.GetString("ggs_service.client_id")
	clientSecret := viper.GetString("ggs_service.client_secret")
	type cred struct {
		ClientID     string   `json:"client_id"`
		ClientSecret string   `json:"client_secret"`
		RedirectURIs []string `json:"redirect_uris"`
		AuthURI      string   `json:"auth_uri"`
		TokenURI     string   `json:"token_uri"`
	}
	var j struct {
		Web       *cred `json:"web"`
		Installed *cred `json:"installed"`
	}
	j.Web = &cred{
		ClientID:     clientId,
		ClientSecret: clientSecret,
		RedirectURIs: []string{
			"urn:ietf:wg:oauth:2.0:oob",
			"http://localhost",
		},
		AuthURI:  AuthURI,
		TokenURI: TokenURI,
	}
	j.Installed = &cred{
		ClientID:     clientId,
		ClientSecret: clientSecret,
		RedirectURIs: []string{
			"urn:ietf:wg:oauth:2.0:oob",
			"http://localhost",
		},
		AuthURI:      AuthURI,
		TokenURI:     TokenURI,
	}

	var c *cred
	switch {
	case j.Web != nil:
		c = j.Web
	case j.Installed != nil:
		c = j.Installed
	default:
		return nil, fmt.Errorf("oauth2/google: no credentials found")
	}
	if len(c.RedirectURIs) < 1 {
		return nil, errors.New("oauth2/google: missing redirect URL in the client_credentials.json")
	}
	return &oauth2.Config{
		ClientID:     c.ClientID,
		ClientSecret: c.ClientSecret,
		RedirectURL:  c.RedirectURIs[0],
		Scopes:       []string{Scope},
		Endpoint: oauth2.Endpoint{
			AuthURL:  c.AuthURI,
			TokenURL: c.TokenURI,
		},
	}, nil
}
