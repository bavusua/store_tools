package gg_services

import (
	"bitbucket.org/bavusua/store_tools/api/models/order"
	"bitbucket.org/bavusua/store_tools/api/models/product"
	"bitbucket.org/bavusua/store_tools/api/models/user"
	"bitbucket.org/bavusua/store_tools/api/utils"
	pkg2 "bitbucket.org/bavusua/store_tools/pkg"
	independent_utils "bitbucket.org/bavusua/store_tools/scheduler/gg_services/independent-utils"
	log "bitbucket.org/bavusua/store_tools/scheduler/pkg/base-log"
	"context"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type GGServiceHandler struct {
	orderManager   *order_model.OrderManager
	productManager *product_model.ProductManager
	userManager    *user_model.UserManager
}

func (h *GGServiceHandler) Process() {
	ctx := context.Background()
	var telesales []*user_model.User
	var lastOrder *order_model.Order
	products, err := h.productManager.Read(ctx, nil)
	if err != nil {
		if !utils.IsNotFound(err) {
			log.Errorw("Could not get product. Detail: %v", err)
		}
		return
	}
	lastOrder, err = h.orderManager.GetLastOrder(ctx)
	if err != nil && !utils.IsNotFound(err) {
		log.Errorw("Could not get product. Detail: %v", err)
		return
	}

	telesales, err = h.userManager.Read(ctx, &user_model.UserRequest{
		Role: utils.ROLE_TELESALES,
		ActiveOnly: true,
	})
	if err != nil {
		log.Errorw("Fail to get telesales", "detail", err)
		return
	}
	foundUpdate := false
	for _, p := range products {
		orders, err := independent_utils.GetNewOrdersInProductSheet(p.GGSheetDetails)
		if err != nil {
			log.Errorw("Fail to read gg sheet", "detail", err)
			return
		}
		if len(orders) == 0 {
			continue
		}

		for _, o := range orders {
			p.GGSheetDetails.LastRowScan++
			o.RowIndex = p.GGSheetDetails.LastRowScan
			if o.PhoneNumber == "" {
				continue //cannot contact for shipment
			}
			next := pkg2.GetNextAssignee(telesales, lastOrder)
			orderPC := pkg2.OrderPackageCommon{h.orderManager, "Auto scan"}
			if err = orderPC.CreateOrder(&order_model.OrderDto{Order: o}, next, p); err != nil {
				log.Errorw("Could not create order", "order", o, "err", err)
				p.GGSheetDetails.LastRowScan--
				break
			}
			lastOrder = o
		}
		foundUpdate = true

		_, err = h.productManager.Update(ctx, p)
		if err != nil {
			log.Errorw("Could not update last row scan", "product", p, "err", err)
		}
	}

	if foundUpdate {
		independent_utils.SendRequestClearCacheProducts()
	}
}

func Init(db *sql.DB) *GGServiceHandler {
	return &GGServiceHandler{
		orderManager:   &order_model.OrderManager{Db: db},
		productManager: &product_model.ProductManager{Db: db},
		userManager:    &user_model.UserManager{Db: db},
	}
}
