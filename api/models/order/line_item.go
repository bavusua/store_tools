package order_model

import (
	"bitbucket.org/bavusua/store_tools/api/models"
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"context"
	"github.com/Masterminds/squirrel"
)
func getItemsTable(ignored ...string) sqlServive.TableData {
	return sqlServive.TableData{
		Name:    "line_items",
		AI:      "id",
		Ignored: ignored,
	}
}

func (m *OrderManager) CreateUpdateInTrans(sqlTool sqlServive.SqlTools, input *LineItem) (err error) {
	var (
		query string
		args []interface{}
		isUpdate = input.Id > 0
	)
	sqlTool.Prepare(getItemsTable(), input, isUpdate)
	if isUpdate {
		query, args = models.GetQueryUpdate(sqlTool, map[string]interface{}{
			"id": input.Id,
		})
	} else {
		query, args = models.GetQueryInsert(sqlTool)
	}
	_, err = sqlTool.ExecTransQuery(query, args...)
	return
}

func (m *OrderManager) DeleteInTrans(sqlTool sqlServive.SqlTools, input *LineItem) (err error) {
	sqlTool.Prepare(getItemsTable(), input, false)
	query := "delete from ? where id = ?"
	_, err = sqlTool.ExecTransQuery(query, sqlTool.GetTableName(), input.Id)
	return
}

func (m *OrderManager) ReadLineItems(ctx context.Context, param *OrderRequest) (items []*LineItem, err error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, &LineItem{}, getItemsTable())
	qb := squirrel.Select(sqlTool.GetColumns()...).From(sqlTool.GetTableName())
	if param.OrderId > 0 {
		qb = qb.Where(squirrel.Eq{"order_id": param.OrderId})
	} else if len(param.OrderIds) > 0 {
		qb = qb.Where(squirrel.Eq{"order_id": param.OrderIds})
	}
	if param.ProductId > 0 {
		qb = qb.Where(squirrel.Eq{"product_id": param.ProductId})
	}
	query, args, _ := qb.ToSql()
	err = sqlTool.Select(&items, query, args...)
	return
}
