package order_model

import (
	"bitbucket.org/bavusua/store_tools/api/models"
	ghnService "bitbucket.org/bavusua/store_tools/api/services/ghn"
)

type Order struct {
	Id              int64                `json:"id,omitempty"`
	CustomerName    string               `json:"customer_name,omitempty"`
	PhoneNumber     string               `json:"phone_number,omitempty"`
	InputAddress    string               `json:"input_address,omitempty"`
	TeleNote        string               `json:"tele_note,omitempty"`
	UserId          int64                `json:"user_id,omitempty"`
	FirstUserId     int64                `json:"first_user_id,omitempty"`
	Logs            []*models.HistoryLog `json:"logs,omitempty"`
	Status          string               `json:"status,omitempty"`
	ShippingStatus  string               `json:"shipping_status,omitempty"`
	LandingPage     string               `json:"landing_page,omitempty"`
	Source          string               `json:"source,omitempty"`
	ProductId       int64                `json:"product_id,omitempty"`
	GHNRequest      *ghnService.GHNOrder `json:"ghn_request,omitempty"`
	ShippingCompany string               `json:"shipping_company,omitempty"`
	ShippingNote    string               `json:"shipping_note,omitempty"`
	ShippingCode    string               `json:"shipping_code,omitempty"`
	ShippingFee     int64                `json:"shipping_fee,omitempty"`
	ShippingAddress string               `json:"shipping_address,omitempty"`
	CustomerIP      string               `json:"customer_ip,omitempty"`
	PlacedAt        int64                `json:"placed_at,omitempty"`
	CreatedAt       int64                `json:"created_at,omitempty"`
	UpdatedAt       int64                `json:"updated_at,omitempty"`
	RowIndex		int64 				 `json:"row_index"`
}

type LineItem struct {
	Id          int64   `json:"id,omitempty"`
	ProductId   int64   `json:"product_id,omitempty"`
	OrderId     int64   `json:"order_id,omitempty"`
	ProductName string  `json:"product_name,omitempty"`
	Quantity    int64   `json:"quantity,omitempty"`
	Price       float64 `json:"price,omitempty"`
	PublishCost float64 `json:"publish_cost"`
}

type OrderDto struct {
	*Order
	LineItems        []*LineItem `json:"line_items,omitempty"`
	LineItemsRemoved []*LineItem `json:"line_items_removed,omitempty"`
}

type OrderRequest struct {
	ShippingCode   string
	ShippingStatus string
	Status         string
	Statuses	   []string
	Ids            []int64
	OrderId        int64
	OrderIds       []int64
	UserId         int64
	PhoneNumber    string
	Source         string
	LandingPage    string
	ProductId      int64
	ProductIds     []int64
	From           int64
	To             int64
}
