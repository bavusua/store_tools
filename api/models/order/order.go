package order_model

import (
	"bitbucket.org/bavusua/store_tools/api/models"
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"bitbucket.org/bavusua/store_tools/api/utils"
	"context"
	"database/sql"
	"github.com/Masterminds/squirrel"
	"github.com/sirupsen/logrus"
	"strings"
	"time"
)

func getOrdersTable(selected ...string) sqlServive.TableData {
	return sqlServive.TableData{
		Name:                 "orders",
		AI:                   "id",
		Datetime:             []string{"created_at", "updated_at", "placed_at"},
		AutoUpdatedDatetime:  []string{"updated_at"},
		AutoInsertedDatetime: []string{"created_at"},
		Selected:              selected,
	}
}

type OrderManager struct {
	Db *sql.DB
}

func (m *OrderManager) CreateDto(ctx context.Context, input *OrderDto) (err error) {
	sqlTool := sqlServive.NewTransactions(ctx, m.Db)
	defer func() {
		if r := recover(); r != nil || err != nil {
			if r != nil {
				logrus.Errorf("Panic reason: %v", r)
			}
			if err != nil {
				logrus.Warnf("Error execute create order: %v", err.Error())
			}
			rerr := sqlTool.RollbackTransactions()
			if rerr != nil {
				if strings.Contains(rerr.Error(), "context canceled") {
					logrus.Warnf("Error when rollback transaction, details: %v", rerr)
				} else {
					logrus.Errorf("Error when rollback transaction, details: %v", rerr)
				}
			}
		}
	}()

	//perform order
	sqlTool.Prepare(getOrdersTable(), input.Order, false)
	query, args := models.GetQueryInsert(sqlTool)
	var rs sql.Result
	var orderId int64
	rs, err = sqlTool.ExecTransQuery(query, args...)
	if err != nil {
		return
	}
	orderId, err = rs.LastInsertId()
	if err != nil {
		return
	}

	//perform line items
	for _, li := range input.LineItems {
		li.OrderId = orderId
		err = m.CreateUpdateInTrans(sqlTool, li)
		if err != nil {
			return
		}
	}

	err = sqlTool.CommitTransactions()
	return
}

func (m *OrderManager) ReadDtos(ctx context.Context, req *OrderRequest) (dtos []*OrderDto, err error) {
	var orders []*Order
	orders, err = m.ReadOrders(ctx, req)
	if err != nil {
		return
	}
	orderIds := make([]int64, 0)
	for _, o := range orders {
		orderIds = append(orderIds, o.Id)
	}

	lineItems, err := m.ReadLineItems(ctx, &OrderRequest{
		OrderIds: orderIds,
	})
	if err != nil {
		return nil, err
	}
	mapLine := make(map[int64][]*LineItem)
	for _, l := range lineItems {
		if _, ok := mapLine[l.OrderId]; !ok {
			mapLine[l.OrderId] = make([]*LineItem, 0)
		}
		mapLine[l.OrderId] = append(mapLine[l.OrderId], l)
	}

	dtos = make([]*OrderDto, 0)
	for _, o := range orders {
		dto := &OrderDto{
			Order: o,
			LineItems: mapLine[o.Id],
		}
		//dto.LineItems, err = m.ReadLineItems(ctx, &OrderRequest{OrderId:o.Id})
		//if err != nil {
		//	return
		//}
		dtos = append(dtos, dto)
	}
	return
}

func (m *OrderManager) UpdateDto(ctx context.Context, input *OrderDto) (err error) {
	sqlTool := sqlServive.NewTransactions(ctx, m.Db)
	defer func() {
		if r := recover(); r != nil || err != nil {
			if r != nil {
				logrus.Errorf("Panic reason: %v", r)
			}
			if err != nil {
				logrus.Warnf("Error execute create order: %v", err.Error())
			}
			rerr := sqlTool.RollbackTransactions()
			if rerr != nil {
				if strings.Contains(rerr.Error(), "context canceled") {
					logrus.Warnf("Error when rollback transaction, details: %v", rerr)
				} else {
					logrus.Errorf("Error when rollback transaction, details: %v", rerr)
				}
			}
		}
	}()

	//perform order
	sqlTool.Prepare(getOrdersTable(), input.Order, false)
	query, args := models.GetQueryUpdate(sqlTool, map[string]interface{}{
		"id": input.Id,
	})
	_, err = sqlTool.ExecTransQuery(query, args...)
	if err != nil {
		return
	}

	//perform line items
	for _, li := range input.LineItems {
		li.OrderId = input.Id
		err = m.CreateUpdateInTrans(sqlTool, li)
		if err != nil {
			return
		}
	}
	for _, li := range input.LineItemsRemoved {
		err = m.DeleteInTrans(sqlTool, li)
		if err != nil {
			return
		}
	}

	err = sqlTool.CommitTransactions()
	return
}

func (m *OrderManager) ReadOrders(ctx context.Context, param *OrderRequest) (orders []*Order, err error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, &Order{}, getOrdersTable())
	qb := squirrel.Select(sqlTool.GetColumns()...).From(sqlTool.GetTableName())
	if param.UserId > 0 {
		qb = qb.Where(squirrel.Eq{"user_id": param.UserId})
	}
	if param.ProductId > 0 {
		qb = qb.Where(squirrel.Eq{"product_id": param.ProductId})
	} else if len(param.ProductIds) > 0 {
		qb = qb.Where(squirrel.Eq{"product_id": param.ProductIds})
	}
	if len(param.Ids) > 0 {
		qb = qb.Where(squirrel.Eq{"id": param.Ids})
	}
	if param.From > 0 {
		qb = qb.Where(squirrel.GtOrEq{"placed_at": time.Unix(param.From, 0)})
	}
	if param.To > 0 {
		qb = qb.Where(squirrel.LtOrEq{"placed_at": time.Unix(param.To, 0)})
	}
	if len(param.ShippingCode) > 0 {
		qb = qb.Where(squirrel.Eq{"shipping_code": param.ShippingCode})
	}
	if len(param.ShippingStatus) > 0 {
		qb = qb.Where(squirrel.Eq{"shipping_status": param.ShippingStatus})
	}
	if len(param.Status) > 0 {
		qb = qb.Where(squirrel.Eq{"status": param.Status})
	}
	if len(param.Statuses) > 0 {
		qb = qb.Where(squirrel.Eq{"status": param.Statuses})
	}

	if len(param.PhoneNumber) > 0 {
		qb = qb.Where(squirrel.Eq{"phone_number": param.PhoneNumber})
	}
	if len(param.Source) > 0 {
		qb = qb.Where(squirrel.Eq{"source": param.Source})
	}
	if len(param.LandingPage) > 0 {
		qb = qb.Where(squirrel.Eq{"landing_page": param.LandingPage})
	}
	query, args, _ := qb.ToSql()
	err = sqlTool.Select(&orders, query, args...)
	return
}

func (m *OrderManager) UpdateOrder(ctx context.Context, input *Order) error {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, input, getOrdersTable())
	err := sqlTool.Update(input)
	return err
}

func (m *OrderManager) CreatingShipment(ctx context.Context, ids []int64) (int64, error) {
	qb := squirrel.Update(getOrdersTable().Name).
		Set("shipping_status", utils.SHIPPING_STT_CREATING).
		Set("updated_at", time.Now().UTC()).
		Where(squirrel.Eq{"id": ids, "status": utils.STATUS_BUYER_CONFIRMED})
	query, args, _ := qb.ToSql()
	rs, err := m.Db.ExecContext(ctx, query, args...)
	if err != nil {
		return 0, err
	}
	return rs.RowsAffected()
}

func (m *OrderManager) GetLastOrder(ctx context.Context) (*Order, error) {
	table := getOrdersTable("id", "first_user_id")
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, &Order{}, table)
	qb := squirrel.Select(sqlTool.GetColumns()...).From(table.Name).
		OrderBy("id desc").Limit(1)
	query, args, _ := qb.ToSql()
	var order Order
	err := sqlTool.Get(ctx, &order, query, args...)
	return &order, err
}

func(m *OrderManager) ReAssign(ctx context.Context, req *OrderRequest) error {
	qb := squirrel.Update(getOrdersTable().Name).
		Set("user_id", req.UserId).
		Set("updated_at", time.Now().UTC()).
		Where(squirrel.Eq{"id": req.Ids})
	query, args, _ := qb.ToSql()
	_, err := m.Db.ExecContext(ctx, query, args...)
	return err
}
