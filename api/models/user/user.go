package user_model

import (
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"context"
	"database/sql"
	"github.com/Masterminds/squirrel"
	"strings"
)

type User struct {
	Id        int64  `json:"id"`
	Username  string `json:"username"`
	Password  string `json:"password"`
	FullName  string `json:"full_name"`
	Role      string `json:"role"`
	IsActive  bool   `json:"is_active"`
	CreatedAt int64  `json:"created_at"`
}

type UserRequest struct {
	Id int64
	Role string
	ActiveOnly bool
}

func getUserTable(ignored... string) sqlServive.TableData {
	return sqlServive.TableData{
		Name:                 "users",
		AI:                   "id",
		Datetime:             []string{"created_at"},
		AutoUpdatedDatetime:  []string{},
		AutoInsertedDatetime: []string{"created_at"},
		Ignored:              ignored,
	}
}

type UserManager struct {
	Db   *sql.DB
}

func (m *UserManager) Create(ctx context.Context, input *User) (*User, error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, input, getUserTable())
	id, err := sqlTool.Insert(input)
	input.Id = id
	return input, err
}

func (m *UserManager) Read(ctx context.Context, req *UserRequest) (users []*User, err error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, &User{}, getUserTable("password"))
	qb := squirrel.Select(sqlTool.GetColumns()...).From(sqlTool.GetTableName())

	if req != nil {
		if req.Id > 0 {
			qb = qb.Where(squirrel.Eq{"id": req.Id})
		}
		if req.Role != "" {
			qb = qb.Where(squirrel.Eq{"role": strings.ToLower(req.Role)})
		}
		if req.ActiveOnly {
			qb = qb.Where(squirrel.Eq{"is_active": true})
		}
	}
	query, args, _ := qb.ToSql()
	err = sqlTool.Select(&users, query, args...)
	return
}

func (m *UserManager) Update(ctx context.Context, input *User) (*User, error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, input, getUserTable())
	err := sqlTool.Update(input)
	return input, err
}

func (m *UserManager) Delete() {

}

func (m *UserManager) GetSignIn (ctx context.Context, input *User) (*User, error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, input, getUserTable())
	query := "select * from users where username = ? and password = ?"
	var user User
	err := sqlTool.Get(ctx, &user, query, input.Username, input.Password)
	return &user, err
}