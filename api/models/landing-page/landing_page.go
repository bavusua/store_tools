package landing_page_model

import (
	"bitbucket.org/bavusua/store_tools/api/models"
	"bitbucket.org/bavusua/store_tools/api/pkg"
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"context"
	"database/sql"
)

type LandingPage struct {
	Url       string `json:"url"`
	UserId    int64  `json:"user_id"`
	Name      string `json:"name"`
	ProductId int64  `json:"product_id"`
	CreatedAt int64  `json:"created_at"`
}

func getLandingPageTable(ignored ...string) sqlServive.TableData {
	return sqlServive.TableData{
		Name:                 "landing_pages",
		Datetime:             []string{"created_at"},
		AutoInsertedDatetime: []string{"created_at"},
		Ignored:              ignored,
	}
}

type LandingPageManager struct {
	Db *sql.DB
}

func (m *LandingPageManager) Create(ctx context.Context, input *LandingPage) (*LandingPage, error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, input, getLandingPageTable())
	_, err := sqlTool.Insert(input)
	return input, err
}

func (m *LandingPageManager) Read(ctx context.Context,
	req *pkg.CommonRequestParams) (lp []*LandingPage, err error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, &LandingPage{}, getLandingPageTable())
	query, args := models.GetQuerySelectFromCommonParam(sqlTool, req)
	err = sqlTool.Select(&lp, query, args...)
	return
}
