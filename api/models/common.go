package models

import (
	"bitbucket.org/bavusua/store_tools/api/pkg"
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"github.com/Masterminds/squirrel"
)

type ShippingOption struct {
	Weight           int64   `json:"weight"`
	Length           int64   `json:"length"`
	Height           int64   `json:"height"`
	Width            int64   `json:"width"`
	OrderRequirement string  `json:"order_requirement"`
	ShippingOption   string  `json:"shipping_option"`
	ShippingNote     string  `json:"shipping_note"`
	HasCostOnPublish bool    `json:"has_cost_on_publish"`
	PublishCost      float64 `json:"publish_cost"`
}

type HistoryLog struct {
	UserName string `json:"user_name"`
	Action   string `json:"action"`
	Note     string `json:"note"`
	Time     int64  `json:"time"`
}

type GGSheetDetails struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	Expiry       int64  `json:"expiry"`

	//old
	Id           string `json:"id"`
	Name         string `json:"name"`
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	LastRowScan  int64  `json:"last_row_scan"`
}

func GetQueryInsert(sqlTool sqlServive.SqlTools) (q string, a []interface{}) {
	qbInsert := squirrel.Insert(sqlTool.GetTableName()).
		Columns(sqlTool.GetColumns()...).Values(sqlTool.GetValues()...)
	q, a, _ = qbInsert.ToSql()
	return
}

func GetQueryUpdate(sqlTool sqlServive.SqlTools, conds map[string]interface{}) (q string, a []interface{}) {
	if conds == nil {
		return
	}
	qbInsert := squirrel.Update(sqlTool.GetTableName()).
		SetMap(func() map[string]interface{} {
			m := make(map[string]interface{})
			for k, f := range sqlTool.GetColumns() {
				m[f] = sqlTool.GetValues()[k]
			}
			return m
		}(),
		).Where(conds)
	q, a, _ = qbInsert.ToSql()
	return
}

func GetQuerySelectFromCommonParam(sqlTool sqlServive.SqlTools, param *pkg.CommonRequestParams) (string, []interface{}) {
	qb := squirrel.Select(sqlTool.GetColumns()...).From(sqlTool.GetTableName())
	if param != nil {
		if param.UserId > 0 {
			qb = qb.Where(squirrel.Eq{"user_id": param.UserId})
		}
		if param.Id > 0 {
			qb = qb.Where(squirrel.Eq{"id": param.Id})
		} else if len(param.Ids) > 0 {
			qb = qb.Where(squirrel.Eq{"id": param.Ids})
		}
		if param.From > 0 {
			qb = qb.Where(squirrel.GtOrEq{"created_at": param.From})
		}
		if param.To > 0 {
			qb = qb.Where(squirrel.LtOrEq{"created_at": param.To})
		}
	}
	q, a, _ := qb.ToSql()
	return q, a
}
