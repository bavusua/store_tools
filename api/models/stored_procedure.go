package models

import (
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"context"
	"database/sql"
	"fmt"
	"time"
)

type ProductAnalytic struct {
	Name           string  `json:"name"`
	InventoryStock int     `json:"inventory_stock"`
	Status         string  `json:"status"`
	WaitGHN        int     `json:"wait_ghn"`
	WaitLocal      int     `json:"wait_local"`
	Budget         int64   `json:"budget"`
	CountYesterday int     `json:"count_yesterday"`
	CountToday     int     `json:"count_today"`
	NoAction       int     `json:"no_action"`
	NoPickUp       int     `json:"no_pick_up"`
	SucceedRate    float64 `json:"succeed_rate"`
	ReturnRate     float64 `json:"return_rate"`
	IncomingStock  int     `json:"incoming_stock"`
}

type TelesaleAnalytic struct {
	Id        int64  `json:"id"`
	FullName  string `json:"full_name"`
	Succeed   int64  `json:"succeed"`
	Reject    int64  `json:"reject"`
	NotPicked int64  `json:"not_picked"`
	Ignored   int64  `json:"ignored"`
	Canceled  int64  `json:"canceled"`
	Returned  int64  `json:"returned"`
}

type Analytics struct {
	Db *sql.DB
}

func (m *Analytics) GetProductAnalytics(ctx context.Context, y, t time.Time) (rs []*ProductAnalytic, err error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, &ProductAnalytic{}, sqlServive.TableData{})
	query := fmt.Sprintf("CALL getAnalyticsProduct(?, ?)")
	err = sqlTool.Select(&rs, query, y, t)
	return
}


func (m *Analytics) GetTelesalesAnalytics(ctx context.Context, t time.Time) (rs []*TelesaleAnalytic, err error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, &TelesaleAnalytic{}, sqlServive.TableData{})
	query := fmt.Sprintf("CALL getAnalyticsTelesale(?)")
	err = sqlTool.Select(&rs, query, t)
	return
}
