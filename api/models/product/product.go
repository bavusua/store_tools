package product_model

import (
	"bitbucket.org/bavusua/store_tools/api/models"
	"bitbucket.org/bavusua/store_tools/api/pkg"
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"context"
	"database/sql"
)

type Product struct {
	Id              int64                  `json:"id"`
	Name            string                 `json:"name"`
	Category        string                 `json:"category"`
	InventoryStock  int64                  `json:"inventory_stock"`
	QuantityInPack  int64                  `json:"quantity_in_pack"`
	ShippingOption  *models.ShippingOption `json:"shipping_option"`
	Cost            float64                `json:"cost"`
	RequestingStock int64                  `json:"requesting_stock"`
	IncomingStock   int64                  `json:"incoming_stock"`
	Budget          float64                `json:"budget"`
	GGSheetDetails  *models.GGSheetDetails `json:"gg_sheet_details"`
	CreatedAt       int64                  `json:"created_at"`
	UpdatedAt       int64                  `json:"updated_at"`
}

func getProductTable(selected ...string) sqlServive.TableData {
	return sqlServive.TableData{
		Name:                 "products",
		AI:                   "id",
		Datetime:             []string{"created_at", "updated_at"},
		AutoUpdatedDatetime:  []string{"updated_at"},
		AutoInsertedDatetime: []string{"created_at"},
		Selected:              selected,
	}
}

type ProductManager struct {
	Db *sql.DB
}

func (m *ProductManager) Create(ctx context.Context, input *Product) (*Product, error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, input, getProductTable())
	id, err := sqlTool.Insert(input)
	input.Id = id
	return input, err
}

func (m *ProductManager) Read(ctx context.Context, req *pkg.CommonRequestParams) (prod []*Product, err error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, &Product{}, getProductTable())
	query, args := models.GetQuerySelectFromCommonParam(sqlTool, req)
	err = sqlTool.Select(&prod, query, args...)
	return
}

func (m *ProductManager) Update(ctx context.Context, input *Product) (*Product, error) {
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, input, getProductTable())
	err := sqlTool.Update(input)
	return input, err
}
func (m *ProductManager) ManageStock(ctx context.Context, input *Product, selectFields []string) (*Product, error) {
	selectFields = append(selectFields, "id")
	sqlTool := sqlServive.NewSqlTools(ctx, m.Db, input, getProductTable(selectFields...))
	err := sqlTool.Update(input)
	return input, err
}

func (m *ProductManager) Delete() {

}
