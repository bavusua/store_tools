package utils

import (
	"database/sql"
	"github.com/spf13/cast"
	"strings"
)

func InStringArray(ar []string, s string) int {
	for i, e := range ar {
		if e == s {
			return i
		}
	}
	return -1
}

func JoinedStringToInt64(input string, spl string) (out []int64) {
	var slc []string
	if spl != "" {
		slc = strings.Split(input, spl)
	} else {
		slc = cast.ToStringSlice(input)
	}
	for _, n := range slc {
		out = append(out, cast.ToInt64(n))
	}
	return
}

func IsNotFound(err error) bool {
	if err == nil {
		return false
	}
	return err.Error() == sql.ErrNoRows.Error()
}