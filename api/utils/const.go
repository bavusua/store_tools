package utils

const (
	ROLE_ADMIN     = "ADMIN"
	ROLE_MARKETING = "MARKETING"
	ROLE_TELESALES = "TELESALES"
	ROLE_KHO       = "WAREHOUSE"
	ROLE_PTSP      = "PRODUCT"
)

type LevelStatus int
const (
	NoAction		= LevelStatus(-1)
	BeforeConfirmed = LevelStatus(1) // telesales check - not include Confirmed
	Confirmed 		= LevelStatus(2)
	CarrierStatus 	= LevelStatus(3)
)

var MapLevelStatus = map[string]LevelStatus {
	STATUS_CREATED: 				NoAction,

	STATUS_DUPLICATED: 				BeforeConfirmed,
	STATUS_WRONG_PHONE_NUMB: 		BeforeConfirmed,
	STATUS_NOT_PICKED_PHONE: 		BeforeConfirmed,
	STATUS_NOT_BUYING: 				BeforeConfirmed,
	STATUS_CANCELED: 				BeforeConfirmed,

	STATUS_BUYER_CONFIRMED: 		Confirmed,

	STATUS_SHIPPED: 				CarrierStatus,
	STATUS_RETURNED: 				CarrierStatus,
	STATUS_DELIVERED: 				CarrierStatus,
	STATUS_DELAYED: 				CarrierStatus,
	STATUS_RETURN: 					CarrierStatus,
}

const (
	//auto scan
	STATUS_CREATED = "CREATED"

	//telesales check
	STATUS_DUPLICATED       = "DUPLICATED"
	STATUS_WRONG_PHONE_NUMB = "WRONG_PHONE_NUMB"
	STATUS_NOT_PICKED_PHONE = "NOT_PICKED_PHONE"
	STATUS_NOT_BUYING       = "NOT_BUYING"
	STATUS_CANCELED         = "CANCELED"
	STATUS_BUYER_CONFIRMED  = "BUYER_CONFIRMED"

	//carrier sync
	STATUS_SHIPPED   = "SHIPPED"
	STATUS_RETURNED  = "RETURNED"
	STATUS_DELIVERED = "DELIVERED"
	STATUS_DELAYED   = "DELAYED"
	STATUS_RETURN    = "RETURN"

	//local carrier
	STATUS_LOCAL_AWAITING_SHIPMENT = "LOCAL_AWAITING_SHIPMENT" //dang giao hang
)

const (
	SHIPPING_STT_CREATING = "CREATING"
	SHIPPING_STT_FAILED   = "FAILED"

	//GHN
	GHN_STT_ReadyToPick     = "ReadyToPick"
	GHN_STT_Picking         = "Picking"
	GHN_STT_Storing         = "Storing"
	GHN_STT_Delivering      = "Delivering"
	GHN_STT_Delivered       = "Delivered"
	GHN_STT_Return          = "Return"
	GHN_STT_Returned        = "Returned"
	GHN_STT_WaitingToFinish = "WaitingToFinish"
	GHN_STT_Finish          = "Finish"
	GHN_STT_Cancel          = "Cancel"
	GHN_STT_LostOrder       = "LostOrder"
)

const (
	SHIPPING_COMPANNY_GHN   = "ghn"
	SHIPPING_COMPANNY_LOCAL = "local"
)

const PublishCostServiceID  = 16
