package main

import (
	"bitbucket.org/bavusua/store_tools/api/services/app-service"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"net/http"
	"store_tools/pkg"
)

func main() {
	InitServer()
}

func InitServer() {
	pkg.InitConfig()
	rt := mux.NewRouter().StrictSlash(true)
	app_service.InitRoutes(rt)
	fmt.Println("Start API Service")
	err := http.ListenAndServe(viper.GetString("port_api"), rt)
	if err != nil {
		panic(err)
	}
}
