package pkg

import "time"

var _globalCache *globalCache
type globalCache struct {
	AuthURL string
	AuthCode string
	CacheManager map[string]*CacheKeyDefinition
}

type CacheKeyDefinition struct {
	Key string
	Expiry time.Duration
	ExpiredTime time.Time
	Value string
}

func init() {
	if _globalCache == nil {
		_globalCache = &globalCache{
			CacheManager: make(map[string]*CacheKeyDefinition),
		}
	}
}

func SetAuthCode (authCode string) {
	_globalCache.AuthURL = ""
	_globalCache.AuthCode = authCode
}

func GetAuthCode() string {
	return _globalCache.AuthCode
}

func SetAuthURL (authUrl string) {
	_globalCache.AuthCode = ""
	_globalCache.AuthURL = authUrl
}

func GetAuthUrl() string {
	return _globalCache.AuthURL
}

// CACHE DEFINES
var (
	AllProductCacheData = CacheKeyDefinition{
		Key: "all-product-cache-data",
		Expiry: time.Hour * 24 * 7, // 7 days
	}
	AllUserCacheData = CacheKeyDefinition{
		Key: "all-user-cache-data",
		Expiry: time.Hour * 24, // 1 days
	}
)

func SetCacheValue(key string, value *CacheKeyDefinition) {
	if value.Value == "" {
		return
	}
	value.ExpiredTime = time.Now().Add(value.Expiry)
	_globalCache.CacheManager[key] = value
}

func GetCacheValue(key string) string {
	val, ok := _globalCache.CacheManager[key]
	if !ok || val == nil {
		return ""
	}
	if time.Now().After(val.ExpiredTime) {
		delete(_globalCache.CacheManager, key)
		return ""
	}

	return val.Value
}

func DeleteCache(key string) {
	if _, ok := _globalCache.CacheManager[key]; !ok {
		return
	}
	delete(_globalCache.CacheManager, key)
}
