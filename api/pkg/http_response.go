package pkg

import (
	"encoding/json"
	"github.com/spf13/viper"
	"net/http"
)

func RespondJSON(w http.ResponseWriter, httpStatusCode int, payload interface{}) {
	data, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	AllowCORS(&w)

	w.WriteHeader(httpStatusCode)
	w.Write(data)
}

func RespondSuccess(w http.ResponseWriter, httpStatusCode int, data interface{}) {
	res := map[string]interface{}{
		"success": true,
		"result": data,
	}
	RespondJSON(w, httpStatusCode, res)
}

func RespondError(w http.ResponseWriter, httpStatusCode int, message string) {
	res := map[string]interface{}{
		"success": false,
		"message": message,
	}
	RespondJSON(w, httpStatusCode, res)
}

func AllowCORS(w *http.ResponseWriter) {
	if viper.GetBool("use_cors") {
		(*w).Header().Set("Content-Type", "application/json; charset=utf-8")
		(*w).Header().Set("Access-Control-Allow-Origin", "*")
		(*w).Header().Set("Access-Control-Allow-Methods", "*")
		(*w).Header().Set("Access-Control-Allow-Headers", "*")
		(*w).Header().Set("Access-Control-Allow-Credentials", "true")
	}
}