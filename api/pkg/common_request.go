package pkg

import (
	"bitbucket.org/bavusua/store_tools/api/utils"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/spf13/cast"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"time"
)

const (
	HEADER_USER_ID   = "SH-USER-ID"
	HEADER_USER_ROLE = "SH-USER-ROLE"
)

var _cachedUsers []*CachedUser

type CommonRequestParams struct {
	Search      string   `json:"search"`
	Page        int      `json:"page"`
	Limit       int      `json:"limit"`
	Id          int64    `json:"id"`
	Status      string   `json:"status"`
	UserId      int64    `json:"user_id"`
	Role        string   `json:"role"`
	Ids         []int64  `json:"ids"`
	From        int64    `json:"from"`
	To          int64    `json:"to"`
	OrderBy     []string `json:"order_by"`
	RequestUser int64    `json:"request_user"`
	ProductId int64 `json:"product_id"`
}

func GetCommonParamsFromRequest(r *http.Request) (p CommonRequestParams) {
	urlQuery := r.URL.Query()
	// ids
	p.RequestUser, p.Role = GetUserFromRequest(r)
	p.UserId = cast.ToInt64(urlQuery.Get("user_id"))
	p.Page = cast.ToInt(urlQuery.Get("page"))
	p.Id = cast.ToInt64(urlQuery.Get("id"))
	p.Status = urlQuery.Get("status")
	p.From = cast.ToInt64(urlQuery.Get("from"))
	p.To = cast.ToInt64(urlQuery.Get("to"))
	p.OrderBy = cast.ToStringSlice(urlQuery.Get("order_by"))
	p.Ids = utils.JoinedStringToInt64(urlQuery.Get("ids"), "")
	p.ProductId = cast.ToInt64(urlQuery.Get("product_id"))

	return
}

func GetUserFromRequest(r *http.Request) (int64, string) {
	id := cast.ToInt64(r.Header.Get(HEADER_USER_ID))
	role := r.Header.Get(HEADER_USER_ROLE)
	return id, role
}

type CachedUser struct {
	UserId      int64
	Username    string
	Role        string
	Token       string
	ExpiredTime int64
}

func GetUserByToken(token string) (user *CachedUser) {
	for _, u := range _cachedUsers {
		if u.Token == token {
			user = u
		}
	}
	return
}

func SetLoggedUser(id int64, username, role string) (token string) {
	token = generateNewToken(username)
	for _, u := range _cachedUsers {
		if u.Username == username {
			u.Token = token
			u.ExpiredTime = time.Now().Add(time.Hour * 96).Unix()
			return
		}
	}
	_cachedUsers = append(_cachedUsers, &CachedUser{
		UserId:      id,
		Username:    username,
		Role:        role,
		Token:       token,
		ExpiredTime: time.Now().Add(time.Hour * 96).Unix(),
	})
	return
}

func DeleteLoggedUser(username string) bool {
	for i, u := range _cachedUsers {
		if u.Username == username {
			_cachedUsers = append(_cachedUsers[:i], _cachedUsers[i+1:]...)
			return true
		}
	}
	return false
}

func generateNewToken(username string) string {
	key := fmt.Sprintf("%v,%v", username, time.Now().Add(time.Hour*13))
	hash, err := bcrypt.GenerateFromPassword([]byte(key), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}

	hasher := md5.New()
	hasher.Write(hash)
	return hex.EncodeToString(hasher.Sum(nil))
}
