package sqlServive

import (
	"bitbucket.org/bavusua/store_tools/api/utils"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/spf13/cast"
	"github.com/spf13/viper"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type TableData struct {
	Name                 string
	AI                   string
	Datetime             []string
	AutoUpdatedDatetime  []string
	AutoInsertedDatetime []string
	Ignored              []string
	Selected             []string
}

type SqlTools struct {
	db               *sql.DB
	tx               *sql.Tx
	table            TableData
	columns          []string
	column2FieldName map[string]string
	column2Kind      map[string]reflect.Kind
	column2Type      map[string]reflect.Type
	values           []interface{}
	ctx              context.Context
	ai               interface{}
}

func InitDBConnection() (*sql.DB, error) {
	var (
		host     = viper.Get("db.host")
		port     = viper.Get("db.port")
		user     = viper.Get("db.user")
		password = viper.Get("db.password")
		schema   = viper.Get("db.schema")
	)
	conn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v",
		user, password, host, port, schema)
	return sql.Open("mysql", conn)
}

func NewTransactions(ctx context.Context, db *sql.DB) (st SqlTools) {
	st.ctx = ctx
	st.db = db

	tx, err := st.db.Begin()
	if err != nil {
		return
	}

	st.tx = tx
	return
}

func (st *SqlTools) RollbackTransactions() error {
	return st.tx.Rollback()
}

func (st *SqlTools) CommitTransactions() error {
	return st.tx.Commit()
}

func (st *SqlTools) Prepare(table TableData, i interface{}, isUpdate bool) {
	st.table = table
	st.parseColumns(i)
	st.fillValues(i, isUpdate)
}

func (st *SqlTools) ExecTransQuery(query string, args ...interface{}) (result sql.Result, err error) {
	stmt, err := st.tx.Prepare(query)
	if err != nil {
		return
	}

	defer stmt.Close()
	result, err = stmt.Exec(args...)
	return
}

func NewSqlTools(ctx context.Context, db *sql.DB, i interface{}, table TableData) SqlTools {
	st := SqlTools{db: db, ctx: ctx, table: table}
	st.parseColumns(i)
	return st
}

func (st *SqlTools) parseColumns(i interface{}) {
	st.columns = make([]string, 0)
	st.column2FieldName = make(map[string]string, 0)
	st.column2Kind = make(map[string]reflect.Kind, 0)
	st.column2Type = make(map[string]reflect.Type, 0)
	st.values = make([]interface{}, 0)

	t := reflect.TypeOf(i).Elem()
	for index := 0; index < t.NumField(); index++ {
		f := t.Field(index)
		jsonPieces := strings.Split(f.Tag.Get("json"), ",")
		column := jsonPieces[0]
		if utils.InStringArray(st.table.Ignored, column) < 0 {
			if len(st.table.Selected) > 0 &&
				utils.InStringArray(st.table.Selected, column) < 0 {
				continue
			}
			st.columns = append(st.columns, column)
			st.column2FieldName[column] = f.Name
			st.column2Kind[column] = f.Type.Kind()
			st.column2Type[column] = f.Type
		}
	}
}

func (st *SqlTools) GetColumns() []string {
	return st.columns
}

func (st *SqlTools) GetValues() []interface{} {
	return st.values
}

func (st *SqlTools) GetTableName() string {
	return st.table.Name
}

func (st *SqlTools) Insert(model interface{}) (id int64, err error) {
	st.fillValues(model, false)
	var rs sql.Result
	argTemp := make([]string, 0)
	for i := 0; i < len(st.values); i++ {
		argTemp = append(argTemp, "?")
	}
	query := fmt.Sprintf("INSERT INTO %v(%v) VALUES(%v)",
		st.table.Name,
		strings.Join(st.columns, ","),
		strings.Join(argTemp, ","))
	rs, err = st.db.ExecContext(st.ctx, query, st.values...)
	if err != nil {
		return
	}
	return rs.LastInsertId()
}

func (st *SqlTools) Update(model interface{}) (err error) {
	st.fillValues(model, false)
	var updates []string
	for _, col := range st.columns {
		updates = append(updates, fmt.Sprintf("%v=?", col))
	}

	query := fmt.Sprintf("UPDATE %v SET %v WHERE id = %v",
		st.table.Name, strings.Join(updates, ","), st.ai)
	_, err = st.db.ExecContext(st.ctx, query, st.values...)
	return
}

func (st *SqlTools) Select(dest interface{}, query string, args ...interface{}) error {
	rows, err := st.db.QueryContext(st.ctx, query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()

	var vp reflect.Value

	value := reflect.ValueOf(dest)

	// json.Unmarshal returns errors for these
	if value.Kind() != reflect.Ptr {
		return errors.New("must pass a pointer, not a value, to StructScan destination")
	}
	if value.IsNil() {
		return errors.New("nil pointer passed to StructScan destination")
	}
	direct := reflect.Indirect(value)

	slice, err := baseType(value.Type(), reflect.Slice)
	if err != nil {
		return err
	}

	isPtr := slice.Elem().Kind() == reflect.Ptr
	base := deref(slice.Elem())

	//empty := true

	for rows.Next() {
		vp = reflect.New(base)
		err = st.scan(rows, vp.Interface())
		if err != nil {
			fmt.Printf(fmt.Sprintf("Error while scan, details: %v", err))
			continue
		}
		//empty = false
		// append
		if isPtr {
			direct.Set(reflect.Append(direct, vp))
		} else {
			direct.Set(reflect.Append(direct, reflect.Indirect(vp)))
		}
	}

	return err
}

func (st *SqlTools) Get(ctx context.Context, dest interface{},
	query string, args ...interface{}) error {
	rows, err := st.db.QueryContext(ctx, query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	var vp reflect.Value

	v := reflect.ValueOf(dest)
	if v.Kind() != reflect.Ptr {
		return errors.New("must pass a pointer, not a value, to StructScan destination")
	}
	if v.IsNil() {
		return errors.New("nil pointer passed to StructScan destination")
	}
	direct := reflect.Indirect(v)

	base := deref(v.Type())
	if rows.Next() {
		vp = reflect.New(base)
		err = st.scan(rows, vp.Interface())
		if err == nil {
			direct.Set(vp.Elem())
		}
	} else {
		err = sql.ErrNoRows
	}

	return err
}

func baseType(t reflect.Type, expected reflect.Kind) (reflect.Type, error) {
	t = deref(t)
	if t.Kind() != expected {
		return nil, fmt.Errorf("expected %s but got %s", expected, t.Kind())
	}
	return t, nil
}

func deref(t reflect.Type) reflect.Type {
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	return t
}

func (st *SqlTools) scan(rows *sql.Rows, dest interface{}) (err error) {
	st.values = make([]interface{}, 0)
	for _, column := range st.columns {
		// get field name
		// fieldName, _ := smth.Column2FieldName[column]
		kind, _ := st.column2Kind[column]
		switch kind {
		case reflect.String:
			var nstr = &sql.NullString{}
			st.values = append(st.values, nstr)
		case reflect.Bool:
			var nbool = &sql.NullBool{}
			st.values = append(st.values, nbool)
		case reflect.Float32:
			var nfloat32 = &sql.NullFloat64{}
			st.values = append(st.values, nfloat32)
		case reflect.Float64:
			var nfloat64 = &sql.NullFloat64{}
			st.values = append(st.values, nfloat64)
		case reflect.Int64, reflect.Int32, reflect.Int:
			if utils.InStringArray(st.table.Datetime, column) < 0 {
				var nint64 = &sql.NullInt64{}
				st.values = append(st.values, nint64)
			} else {
				var ntime = &NullTime{}
				st.values = append(st.values, ntime)
			}
		case reflect.Slice, reflect.Struct, reflect.Map, reflect.Ptr:
			var nbytes = &sql.RawBytes{}
			st.values = append(st.values, nbytes)
		}
	}
	err = rows.Scan(st.values...)
	if err != nil {
		fmt.Printf("Sql tool: Scan error: %v ", err)
		return
	}

	v := reflect.ValueOf(dest)
	// log.Print(v.Kind())
	if v.Kind() != reflect.Ptr {
		return errors.New("must pass a pointer, not a value, to StructScan destination")
	}
	if v.IsNil() {
		return errors.New("nil pointer passed to StructScan destination")
	}

	ve := v.Elem()
	// log.Print(ve.Kind())
	for index, column := range st.columns {
		// get field name
		fieldName, _ := st.column2FieldName[column]
		kind, _ := st.column2Kind[column]
		value := st.values[index]

		switch kind {
		case reflect.String:
			ve.FieldByName(fieldName).SetString(value.(*sql.NullString).String)
		case reflect.Bool:
			ve.FieldByName(fieldName).SetBool(value.(*sql.NullBool).Bool)
		case reflect.Float64:
			ve.FieldByName(fieldName).SetFloat(value.(*sql.NullFloat64).Float64)
		case reflect.Int64:
			if utils.InStringArray(st.table.Datetime, column) < 0 {
				ve.FieldByName(fieldName).SetInt(value.(*sql.NullInt64).Int64)
			} else {
				var vtime = value.(*NullTime)
				if vtime.Valid && vtime.Time.Unix() >= 0 {
					ve.FieldByName(fieldName).SetInt(vtime.Time.Unix())
				}
			}
		case reflect.Int32, reflect.Int:
			ve.FieldByName(fieldName).SetInt(value.(*sql.NullInt64).Int64)
		case reflect.Slice:
			val := value.(*sql.RawBytes)

			// If the slice is not slice of bytes
			if ve.FieldByName(fieldName).Type().Elem().Kind() != reflect.Uint8 {
				if len(*val) < 1 {
					break
				}
				v := make([]interface{}, 0)
				s := reflect.MakeSlice(st.column2Type[column], 0, 0)
				if err = json.Unmarshal(*val, &v); err == nil {
					s, _ = scanWithRecursive(v, s, st.column2Type[column].Elem())
					ve.FieldByName(fieldName).Set(s)
				} else {
					fmt.Print(err)
				}
			} else {
				ve.FieldByName(fieldName).SetBytes(*val)
			}
		case reflect.Map, reflect.Ptr:
			val := value.(*sql.RawBytes)

			// If the slice is not slice of bytes
			if ve.FieldByName(fieldName).Type().Elem().Kind() != reflect.Uint8 {
				if len(*val) < 1 {
					break
				}

				v := make(map[string]interface{}, 0)
				if err = json.Unmarshal(*val, &v); err == nil {
					dataValue := reflect.New(deref(st.column2Type[column]))
					scanWithRecursive(v, dataValue.Elem(), st.column2Type[column].Elem())
					ve.FieldByName(fieldName).Set(dataValue)
				} else {
					fmt.Print(err)
				}
			}
		}
	}

	return
}

func scanWithRecursive(data interface{}, ve reflect.Value, sliceType reflect.Type) (res reflect.Value, err error) {
	switch document := data.(type) {
	case map[string]interface{}:
		numField := ve.NumField()
		for i := 0; i < numField; i++ {
			sliceTag := strings.Split(ve.Type().Field(i).Tag.Get("json"), ",")
			if (len(sliceTag) > 0 && sliceTag[0] == "-") || len(sliceTag) == 0 {
				continue
			}

			destinationFName := ve.Type().Field(i).Name
			documentFieldName := sliceTag[0]
			var documentFieldValue interface{}
			var documentHasField bool
			if documentFieldValue, documentHasField = document[documentFieldName];
				!documentHasField || documentFieldValue == nil {
				continue
			}
			destinationFVReflect := ve.FieldByName(destinationFName)
			if !destinationFVReflect.CanSet() {
				continue
			}

			fieldValueString := cast.ToString(documentFieldValue)
			switch destinationFVReflect.Kind() {
			case reflect.Int64:
				if v, err := strconv.ParseInt(fieldValueString, 10, 64); err == nil {
					ve.FieldByName(destinationFName).SetInt(v)
				}
			case reflect.Float64:
				if v, err := strconv.ParseFloat(fieldValueString, 64); err == nil {
					ve.FieldByName(destinationFName).SetFloat(v)
				}
			case reflect.Slice:
				if v, ok := documentFieldValue.([]interface{}); v != nil && ok {
					embeddedDocuments := reflect.MakeSlice(destinationFVReflect.Type(), 0, 0)
					kind := destinationFVReflect.Type().Elem().Kind()
					if reflect.TypeOf(documentFieldValue).Kind() == reflect.Slice {
						for i := 0; i <= len(v)-1; i++ {
							var dataElement reflect.Value
							switch kind {
							case reflect.Slice:
								dataElement := reflect.New(deref(destinationFVReflect.Type().Elem()))
								scanWithRecursive(v[i], dataElement.Elem(), dataElement.Type().Elem())
							case reflect.Map, reflect.Ptr:
								dataElement = reflect.New(deref(destinationFVReflect.Type().Elem()))
								scanWithRecursive(v[i], dataElement.Elem(), nil)
							default:
								dataElement = reflect.ValueOf(correctDataType(v[i], destinationFVReflect.Type().Elem().Kind()))
								//dataElement = reflect.ValueOf(v[i])
							}

							embeddedDocuments = reflect.Append(embeddedDocuments, dataElement)
						}
						ve.FieldByName(destinationFName).Set(embeddedDocuments)
					}
				}
			case reflect.Map:
				if reflect.TypeOf(documentFieldValue).Kind() == reflect.Map {
					mapField := ve.FieldByName(destinationFName)
					mapField.Set(reflect.MakeMap(mapField.Type()))
					mapValue, ok := documentFieldValue.(map[string]interface{})
					if ok {
						for mapK, mapV := range mapValue {
							dataElement := reflect.New(destinationFVReflect.Elem().Type())
							scanWithRecursive(mapV, dataElement, nil)
							mapField.SetMapIndex(reflect.ValueOf(mapK), dataElement)
						}
					}
				}
			case reflect.Ptr:
				if reflect.TypeOf(documentFieldValue).Kind() == reflect.Map {
					if v, ok := documentFieldValue.(map[string]interface{}); ok {
						dataValue := reflect.New(destinationFVReflect.Type().Elem())
						_, err = scanWithRecursive(v, dataValue.Elem(), nil)
						ve.FieldByName(destinationFName).Set(dataValue)
					}

				}
			default:
				if vCorrected := correctDataType(documentFieldValue, destinationFVReflect.Kind()); vCorrected != nil {
					ve.FieldByName(destinationFName).Set(reflect.ValueOf(vCorrected))
				}
			}
		}
	case []interface{}:
		if ve.Kind() == reflect.Slice {
			for i := 0; i <= len(document)-1; i++ {
				switch sliceType.Kind() {
				case reflect.String, reflect.Int64, reflect.Int32, reflect.Int8, reflect.Float64, reflect.Float32:
					ve = reflect.Append(ve, reflect.ValueOf(correctDataType(document[i], ve.Type().Elem().Kind())))
				case reflect.Slice, reflect.Struct, reflect.Ptr, reflect.Map:
					dataElement := reflect.New(sliceType.Elem())
					scanWithRecursive(document[i], dataElement.Elem(), nil)
					ve = reflect.Append(ve, dataElement)
				}
			}
			res = ve
		}
	}
	return
}

func correctDataType(v interface{}, dataType reflect.Kind) interface{} {
	if v == nil {
		return nil
	}
	switch dataType {
	case reflect.String:
		return cast.ToString(v)
	case reflect.Int64:
		switch reflect.ValueOf(v).Kind() {
		case reflect.Ptr:
			if vi, ok := v.(*time.Time); ok {
				return vi.Unix()
			}
		case reflect.Struct:
			if vi, ok := v.(time.Time); ok {
				return vi.Unix()
			}
		}
		return cast.ToInt64(v)
	case reflect.Int32:
		return cast.ToInt32(v)
	case reflect.Float64:
		return cast.ToFloat64(v)
	case reflect.Float32:
		return cast.ToFloat32(v)
	case reflect.Uint64:
		return cast.ToUint64(v)
	case reflect.Uint32:
		return cast.ToUint32(v)
	case reflect.Bool:
		return cast.ToBool(v)
	}
	return nil
}

func (st *SqlTools) fillValues(i interface{}, isUpdate bool) {
	st.values = make([]interface{}, 0)
	v := reflect.ValueOf(i).Elem()
	for _, column := range st.columns {
		// get field name
		fieldName, _ := st.column2FieldName[column]
		// kind, _ := smth.Column2Kind[column]
		fieldValue := v.FieldByName(fieldName)
		fieldValueInterface := fieldValue.Interface()
		convertedValue := fieldValueInterface

		// check if column is datetime
		if utils.InStringArray(st.table.Datetime, column) >= 0 {
			convertedValueInt := cast.ToInt64(convertedValue)
			create := append(st.table.AutoUpdatedDatetime, st.table.AutoInsertedDatetime...)
			if convertedValueInt < 1 &&
				(!isUpdate && utils.InStringArray(create, column) >= 0 ||
					utils.InStringArray(st.table.AutoUpdatedDatetime, column) >= 0) {
				convertedValue = time.Now()

			} else {
				if IsZeroOfUnderlyingType(fieldValueInterface) {
					convertedValue = nil
				} else {
					convertedValue = time.Unix(fieldValue.Int(), 0)
				}
			}
		}

		if st.column2Kind[column] == reflect.Slice && st.column2Type[column].Elem().Kind() == reflect.Uint8 {
			st.values = append(st.values, convertedValue)
			continue
		}

		var errMarshal error
		switch st.column2Kind[column] {
		case reflect.Slice, reflect.Struct, reflect.Ptr, reflect.Map:
			if IsZeroOfUnderlyingType(fieldValueInterface) {
				convertedValue = nil
			} else {
				convertedValue, errMarshal = json.Marshal(convertedValue)
			}
		}
		if errMarshal != nil {
			panic(errMarshal)
		}

		st.values = append(st.values, convertedValue)
	}
	st.removeAIField()
}

// IsZeroOfUnderlyingType --
func IsZeroOfUnderlyingType(x interface{}) bool {
	return reflect.DeepEqual(x, reflect.Zero(reflect.TypeOf(x)).Interface())
}

func (st *SqlTools) removeAIField() {
	tmpColumns := make([]string, 0)
	tmpValues := make([]interface{}, 0)
	for index, column := range st.columns {
		if column == st.table.AI {
			st.ai = st.values[index]
			continue
		}
		tmpColumns = append(tmpColumns, column)
		tmpValues = append(tmpValues, st.values[index])
	}
	st.columns = tmpColumns
	st.values = tmpValues

}
