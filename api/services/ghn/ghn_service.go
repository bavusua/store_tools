package ghnService

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	PaymentTypeID_SHOP_PAY  = 1
	PaymentTypeID_Buyer_PAY = 2

	NoteCode_CHOTHUHANG         = "CHOTHUHANG"
	NoteCode_CHOXEMHANGKHONGTHU = "CHOXEMHANGKHONGTHU"
	NoteCode_KHONGCHOXEMHANG    = "KHONGCHOXEMHANG"

	MAX_InsuranceFeeoptional = 10000000
	MAX_CoDAmount            = 50000000

	ContentTypeJSON = "application/json"

	domain = "https://console.ghn.vn"
)

// Request
func CreateOrder(params GHNOrder) (POSTResponse, error) {
	return requestWithParams("/api/v1/apiv3/CreateOrder", params)
}

func GetWardByDistrictId(id int) (POSTResponse, error) {
	return requestWithParams("/api/v1/apiv3/GetWards", map[string]int{
		"DistrictID": id,
	})
}

func GetServices(params map[string]interface{}) (POSTResponse, error) {
	return requestWithParams("/api/v1/apiv3/FindAvailableServices", params)
}

func CalculateFee(params map[string]interface{}) (POSTResponse, error) {
	return requestWithParams("/api/v1/apiv3/CalculateFee", params)
}

func ToStringResponseData(rs POSTResponse) string {
	b, err := json.Marshal(rs.Data)
	if err != nil {
		return ""
	} else {
		return string(b)
	}
}

func requestWithParams(path string, params interface{}) (response POSTResponse, err error) {
	var res *http.Response
	bodyContent := getBodyContent(params)
	fullPath := domain + path
	res, err = http.Post(fullPath, ContentTypeJSON, strings.NewReader(bodyContent))
	if err != nil {
		return
	}
	if res.Body == nil {
		err = fmt.Errorf("No response")
		return
	}
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(data, &response)
	if err != nil {
		return
	}
	if response.Code == 0 || res.StatusCode > 300 || res.StatusCode < 200 {
		err = fmt.Errorf(response.Msg)
	}
	return
}

func getBodyContent(params interface{}) (bodyContent string) {
	var mapParams map[string]interface{}
	inrec, _ := json.Marshal(params)
	if json.Unmarshal(inrec, &mapParams) != nil {
		return
	}
	mapParams["token"] = viper.Get("ghn_service.token")
	if bodyBytes, err := json.Marshal(mapParams); err == nil {
		bodyContent = string(bodyBytes)
		if string(bodyContent[0]) == `"` {
			err = json.Unmarshal([]byte(bodyContent), &bodyContent)
			if err != nil {
				fmt.Print(err)
			}
		}
	}
	return
}

// Struct
// Note: omitempty means optional fields
type POSTResponse struct {
	Code int                    `json:"code"`
	Msg  string                 `json:"msg"`
	Data interface{} `json:"data"`
}

type ServiceList struct {
	ExpectedDeliveryTime string `json:"ExpectedDeliveryTime"`
	Extras []struct{
		MaxValue int `json:"MaxValue"`
		Name string `json:"Name"`
		ServiceFee int64 `json:"ServiceFee"`
		ServiceID int64 `json:"ServiceID"`
	} `json:"Extras"`
	Name string `json:"Name"`
	ServiceFee int64 `json:"ServiceFee"`
	ServiceID int64 `json:"ServiceID"`
}

type GHNOrder struct {
	Token                string `json:"token"`
	PaymentTypeID        int64   `json:"PaymentTypeID,omitempty"`
	FromDistrictID       int64   `json:"FromDistrictID"`
	FromWardCode         string  `json:"FromWardCode"`
	ToDistrictID         int64   `json:"ToDistrictID"`
	ToWardCode           string  `json:"ToWardCode"`
	ClientContactName    string  `json:"ClientContactName"`
	ClientContactPhone   string  `json:"ClientContactPhone"`
	ClientAddress        string  `json:"ClientAddress"`
	CustomerName         string  `json:"CustomerName"`
	CustomerPhone        string  `json:"CustomerPhone"`
	ShippingAddress      string  `json:"ShippingAddress"`
	NoteCode             string  `json:"NoteCode"`
	ClientHubID          int64     `json:"ClientHubID,omitempty"`
	ServiceID            int64   `json:"ServiceID"`
	Weight               int64   `json:"Weight"` //gram
	Length               int64   `json:"Length"` //cm
	Width                int64   `json:"Width"`  //cm
	Height               int64   `json:"Height"` //cm
	InsuranceFee         int64   `json:"InsuranceFee,omitempty"`
	CoDAmount            int64   `json:"CoDAmount,omitempty"`
	Note                 string  `json:"Note,omitempty"`
	SealCode             string  `json:"SealCode,omitempty"`
	ExternalCode         string  `json:"ExternalCode,omitempty"`
	ToLatitude           float64 `json:"ToLatitude,omitempty"`
	ToLongitude          float64 `json:"ToLongitude,omitempty"`
	FromLat              float64 `json:"FromLat,omitempty"`
	FromLng              float64 `json:"FromLng,omitempty"`
	Content              string  `json:"Content,omitempty"`
	CouponCode           string  `json:"CouponCode,omitempty"`
	CheckMainBankAccount bool    `json:"CheckMainBankAccount,omitempty"`
	ShippingOrderCosts   []*ShippingOrderCost `json:"ShippingOrderCosts,omitempty"`
	ReturnContactName  string `json:"ReturnContactName"`
	ReturnContactPhone string `json:"ReturnContactPhone"`
	ReturnAddress      string `json:"ReturnAddress"`
	ReturnDistrictID   int64    `json:"ReturnDistrictID"`
	ExternalReturnCode string `json:"ExternalReturnCode"`
	IsCreditCreate     bool   `json:"IsCreditCreate,omitempty"`
	AffiliateID        int64    `json:"AffiliateID"`

	//returned data
	CurrentStatus string `json:"CurrentStatus"`
	CurrentWarehouseName string `json:"CurrentWarehouseName"`
	CustomerID int64 `json:"CustomerID"`
	ServiceName string `json:"ServiceName"`
	OrderCode string `json:"OrderCode"`
}

type ShippingOrderCost struct {
	ServiceID int64 `json:"ServiceID"`
}