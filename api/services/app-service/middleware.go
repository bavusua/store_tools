package app_service

import (
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"bitbucket.org/bavusua/store_tools/api/utils"
	"github.com/spf13/cast"
	"net/http"
	"store_tools/api/pkg"
	"strings"
	"time"
)

const token_key = "SH-AUTH-TOKEN"

func middleware(next http.Handler, roles []string, noAuth bool) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		if (*r).Method == "OPTIONS" {
			pkg.AllowCORS(&w)
			return
		}
		if isDevByPass(r) || noAuth {
			next.ServeHTTP(w, r)
		} else {
			id, str := validateToken(r)
			if id == 0 {
				httpClient.RespondError(w, http.StatusUnauthorized, str)
				return
			}

			usrRole := strings.ToUpper(str)
			if usrRole != utils.ROLE_ADMIN && utils.InStringArray(roles, usrRole) < 0 {
				httpClient.RespondError(w, http.StatusUnauthorized, "Unauthorized")
				return
			}
			r.Header.Set(httpClient.HEADER_USER_ID, cast.ToString(id))
			r.Header.Set(httpClient.HEADER_USER_ROLE, usrRole)
			next.ServeHTTP(w, r)
		}
	})
}

func validateToken(request *http.Request) (int64, string) {
	token := request.Header.Get(token_key)
	lu := httpClient.GetUserByToken(token)
	if lu == nil {
		return 0, "Invalid token"
	}
	if lu.ExpiredTime < time.Now().Unix() {
		return 0, "Token expired"
	}
	return lu.UserId, lu.Role
}

func isDevByPass(request *http.Request) bool {
	return request.URL.Query().Get("by_pass_key") == "1"
}
