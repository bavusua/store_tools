package app_service

import (
	"bitbucket.org/bavusua/store_tools/api/handlers/analytics"
	ggs_access "bitbucket.org/bavusua/store_tools/api/handlers/ggs-access"
	"bitbucket.org/bavusua/store_tools/api/handlers/ghn-service"
	"bitbucket.org/bavusua/store_tools/api/handlers/order"
	"bitbucket.org/bavusua/store_tools/api/handlers/product"
	"bitbucket.org/bavusua/store_tools/api/handlers/user"
	"bitbucket.org/bavusua/store_tools/api/models/landing-page"
	order_model "bitbucket.org/bavusua/store_tools/api/models/order"
	"bitbucket.org/bavusua/store_tools/api/models/product"
	"bitbucket.org/bavusua/store_tools/api/models/user"
	sqlServive "bitbucket.org/bavusua/store_tools/api/services/sql"
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"store_tools/api/models"
	"store_tools/api/utils"
)

type serve struct {
	userManager        *user_model.UserManager
	orderManager       *order_model.OrderManager
	productManager     *product_model.ProductManager
	landingPageManager *landing_page_model.LandingPageManager
	analytics          *models.Analytics
}
type RouteHandle struct {
	Path    string
	Handler http.Handler
	Method  string
	Roles   []string
	NoAuth  bool
}

type APIHandler struct {
	Db *sql.DB
}

var s = &serve{}

func RegisterRoutes() []RouteHandle {
	return []RouteHandle{
		//ghn
		{
			Path: "/ghn-create-order",
			Handler: &ghn_service.GHNCreateOrderHandle{
				OrderManager: s.orderManager,
				ProductManager: s.productManager,
			},
			Method: http.MethodPost,
			Roles:  []string{utils.ROLE_KHO},
		},
		{
			Path:    "/ghn-get-wards",
			Handler: &ghn_service.GHNGetWardsHandle{},
			Method:  http.MethodGet,
			NoAuth:  true,
		},
		{
			Path: "/ghn-webhook",
			Handler: &ghn_service.GHNWebhookHandle{
				OrderManager: s.orderManager,
			},
			Method: http.MethodPost,
			NoAuth: true,
		},

		//order
		{
			Path: "/update-order",
			Handler: &order.OrdersUpdateHandler{
				OrderManager: s.orderManager,
				ProductManager: s.productManager,
			},
			Method: http.MethodPut,
			Roles:  []string{utils.ROLE_TELESALES},
		},
		{
			Path: "/orders",
			Handler: &order.OrdersGetHandler{
				OrderManager: s.orderManager,
			},
			Method: http.MethodGet,
			NoAuth: true,
		},
		{
			Path: "/import-orders",
			Handler: &order.OrdersImportHandler{
				OrderManager: s.orderManager,
			},
			Method: http.MethodPost,
		},
		{
			Path: "/reassign-orders",
			Handler: &order.OrdersReAssignHandler{
				OrderManager: s.orderManager,
			},
			Method: http.MethodPut,
		},

		//product
		{
			Path: "/products",
			Handler: &product.ProductsGetHandler{
				ProductManager: s.productManager,
			},
			Method: http.MethodGet,
			NoAuth: true,
		},
		{
			Path: "/create-update-product",
			Handler: &product.ProductsCreateUpdateHandler{
				ProductManager: s.productManager,
			},
			Method: http.MethodPost,
			Roles:  []string{utils.ROLE_MARKETING, utils.ROLE_KHO, utils.ROLE_PTSP},
		},
		{
			Path: "/products/cache",
			Handler: &product.ClearCacheHandler{},
			Method: http.MethodPost,
			NoAuth: true,
		},
		{
			Path: "/landing-pages",
			Handler: &product.LandingPagesGetHandler{
				LandingPageManager: s.landingPageManager,
			},
			Method: http.MethodGet,
			Roles:  []string{utils.ROLE_MARKETING},
		},
		{
			Path: "/landing-page",
			Handler: &product.LandingPagesCreateHandler{
				LandingPageManager: s.landingPageManager,
			},
			Method: http.MethodPost,
			Roles:  []string{utils.ROLE_MARKETING},
		},
		{
			Path: "/manage-stock",
			Handler: &product.ProductsManageStockHandler{
				ProductManager: s.productManager,
			},
			Method: http.MethodPost,
			Roles:  []string{utils.ROLE_MARKETING, utils.ROLE_PTSP},
		},

		//user
		{
			Path: "/users",
			Handler: &user.UsersGetHandler{
				UserManager: s.userManager,
			},
			Method: http.MethodGet,
			Roles:  []string{utils.ROLE_MARKETING, utils.ROLE_KHO, utils.ROLE_PTSP, utils.ROLE_TELESALES},
		},
		{
			Path: "/create-update-user",
			Handler: &user.UsersCreateUpdateHandler{
				UserManager: s.userManager,
			},
			Method: http.MethodPost,
		},
		{
			Path: "/sign-in",
			Handler: &user.UsersSignInHandler{
				UserManager: s.userManager,
			},
			Method: http.MethodPost,
			NoAuth: true,
		},
		{
			Path:    "/sign-out",
			Handler: &user.UsersSignOutHandler{},
			Method:  http.MethodPost,
			NoAuth:  true,
		},

		// analytics
		{
			Path: "/analytics/product",
			Handler: &analytics.GetProductAnalyticsHandler{
				Analytics: s.analytics,
			},
			Method: http.MethodGet,
			Roles:  []string{utils.ROLE_MARKETING, utils.ROLE_KHO, utils.ROLE_PTSP},
		},
		{
			Path: "/analytics/telesales",
			Handler: &analytics.GetTelesalesAnalyticsHandler{
				Analytics: s.analytics,
			},
			Method: http.MethodGet,
			Roles:  []string{utils.ROLE_MARKETING, utils.ROLE_KHO, utils.ROLE_PTSP},
		},
		{
			Path:    "/get-auth-url",
			Handler: &ggs_access.GgsGetAuthUrlHandler{},
			Method:  http.MethodGet,
			NoAuth:  true,
		},
		{
			Path:    "/get-auth-code",
			Handler: &ggs_access.GgsGetAuthCodeHandler{},
			Method:  http.MethodGet,
			NoAuth:  true,
		},
		{
			Path:    "/set-auth-url",
			Handler: &ggs_access.GgsSetAuthUrlHandler{},
			Method:  http.MethodPost,
			NoAuth:  true,
		},
		{
			Path:    "/set-auth-code",
			Handler: &ggs_access.GgsSetAuthCodeHandler{},
			Method:  http.MethodPost,
			NoAuth:  true,
		},
	}
}

func InitRoutes(appRouting *mux.Router) {
	fmt.Println("Initiated ")
	db, err := sqlServive.InitDBConnection()
	if err != nil {
		log.Print(err)
	}

	s.orderManager = &order_model.OrderManager{Db: db}
	s.userManager = &user_model.UserManager{Db: db}
	s.productManager = &product_model.ProductManager{Db: db}
	s.landingPageManager = &landing_page_model.LandingPageManager{Db: db}
	s.analytics = &models.Analytics{Db: db}
	s.AddRoute(appRouting)
}

func (m *serve) AddRoute(appRouting *mux.Router) {
	for _, r := range RegisterRoutes() {
		appRouting.Handle(r.Path,
			middleware(http.HandlerFunc(r.Handler.ServeHTTP), r.Roles, r.NoAuth)).
			Methods(r.Method, http.MethodOptions)
	}
}
