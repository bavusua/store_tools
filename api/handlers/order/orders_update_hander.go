package order

import (
	"bitbucket.org/bavusua/store_tools/api/models/order"
	product_model "bitbucket.org/bavusua/store_tools/api/models/product"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	pkg2 "bitbucket.org/bavusua/store_tools/api/pkg"
	ghnService "bitbucket.org/bavusua/store_tools/api/services/ghn"
	"bitbucket.org/bavusua/store_tools/api/utils"
	"encoding/json"
	"fmt"
	"github.com/spf13/cast"
	"net/http"
	"store_tools/pkg"
)

type OrdersUpdateHandler struct {
	OrderManager  *order_model.OrderManager
	ProductManager *product_model.ProductManager
}

func (h *OrdersUpdateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	form := order_model.OrderDto{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&form)
	if err != nil {
		fmt.Print(err)
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}

	var (
		errStr string
	)
	switch form.ShippingCompany {
	case utils.SHIPPING_COMPANNY_GHN:
		if form.Status != utils.STATUS_BUYER_CONFIRMED {
			break
		}
		products, err := h.ProductManager.Read(ctx, &pkg2.CommonRequestParams{
			Id: form.ProductId,
		})
		if err != nil || len(products) <= 0 {
			errStr = fmt.Sprintf("Not found product, error: %v", err)
			break
		}
		var (
			total float64
			totalPublish float64
		)
		for _, li := range form.LineItems {
			total += li.Price
			totalPublish += li.PublishCost
		}
		if form.GHNRequest.FromDistrictID <= 0 {
			pkg.TransformGHNBaseRequest(&form, products[0])
		}
		form.ShippingFee, errStr = CalculateGHNShippingService(form.GHNRequest, total, totalPublish)
		form.ShippingStatus = "" // confirm -> reset shipping status

	case utils.SHIPPING_COMPANNY_LOCAL:
		if form.ShippingStatus == "" {
			break
		}
		if utils.MapLevelStatus[form.Status] >= utils.Confirmed {
			form.Status = form.ShippingStatus
		} else {
			form.ShippingStatus = ""
		}
	default:
		fmt.Println("Unknown company")
	}
	if errStr != "" {
		httpClient.RespondError(w, http.StatusInternalServerError, errStr)
		return
	}

	err = h.OrderManager.UpdateDto(ctx, &form)
	if err != nil {
		fmt.Printf("Could not update order. Detail: %v", err)
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, form)
	}
	return
}

func CalculateGHNShippingService(ghn *ghnService.GHNOrder,
	total, totalPublish float64) (fee int64, stringErr string) {
	params := map[string]interface{}{
		"ToDistrictID":   ghn.ToDistrictID,
		"FromDistrictID": ghn.FromDistrictID,
		"Weight":         ghn.Weight,
		"Length":         ghn.Length,
		"Width":          ghn.Width,
		"Height":         ghn.Height,
	}
	rs, err := ghnService.GetServices(params)
	if err != nil {
		stringErr = fmt.Sprintf("%v: %v", err.Error(), ghnService.ToStringResponseData(rs))
		return
	}
	data, _ := json.Marshal(rs.Data)
	var services []ghnService.ServiceList
	err = json.Unmarshal(data, &services)
	if err != nil {
		fmt.Printf("Could not process ghn shipment. Detail: %v", err)
		stringErr = err.Error()
		return
	}

	for _, sv := range services {
		if sv.Name == ghn.ServiceName {
			ghn.ServiceID = sv.ServiceID
			ghn.CoDAmount = int64(total)
			ghn.InsuranceFee = int64(totalPublish)
			if totalPublish > 0 {
				params["ServiceID"] = sv.ServiceID
				params["InsuranceFee"] = int64(totalPublish)
				params["OrderCosts"] = []*ghnService.ShippingOrderCost{
					{ServiceID: utils.PublishCostServiceID},
				}
				rs, err = ghnService.CalculateFee(params)
				if err != nil {
					stringErr = fmt.Sprintf("%v: %v", err.Error(), ghnService.ToStringResponseData(rs))
					return
				}
				data, _ = json.Marshal(rs.Data)
				var feeDetail map[string]interface{}
				err = json.Unmarshal(data, &feeDetail)
				if err != nil {
					stringErr = fmt.Sprintf("%v: %v", err.Error(), ghnService.ToStringResponseData(rs))
					return
				}
				fee =  cast.ToInt64(feeDetail["CalculatedFee"])
			} else {
				fee = sv.ServiceFee
			}

			break
		}
	}
	return
}
