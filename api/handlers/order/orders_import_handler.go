package order

import (
	order_model "bitbucket.org/bavusua/store_tools/api/models/order"
	products2 "bitbucket.org/bavusua/store_tools/api/models/product"
	user_model "bitbucket.org/bavusua/store_tools/api/models/user"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"bitbucket.org/bavusua/store_tools/api/utils"
	"bitbucket.org/bavusua/store_tools/pkg"
	base_log "bitbucket.org/bavusua/store_tools/scheduler/pkg/base-log"
	"encoding/json"
	"fmt"
	"net/http"
)

type orderRequest struct {
	Order       *order_model.Order `json:"order"`
	ProductName string             `json:"product_name"`
}

type ImportOrderRequest struct {
	Data []*orderRequest
}

type OrdersImportHandler struct {
	OrderManager   *order_model.OrderManager
	ProductManager *products2.ProductManager
	UserManager    *user_model.UserManager
}

func (h *OrdersImportHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	form := ImportOrderRequest{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&form)
	if err != nil || form.Data == nil || len(form.Data) == 0 {
		base_log.Errorw("error import orders", err)
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}

	var (
		products  []*products2.Product
		users     []*user_model.User
		lastOrder *order_model.Order
	)
	err = func() error {
		res, scopedErr := h.ProductManager.Read(ctx, nil)
		if scopedErr != nil {
			if !utils.IsNotFound(scopedErr) {
				base_log.Errorw("Could not get products. Detail: %v", scopedErr)
			}
			return scopedErr
		}
		products = res

		resUser, scopedErr := h.UserManager.Read(ctx, &user_model.UserRequest{
			Role:       utils.ROLE_TELESALES,
			ActiveOnly: true,
		})
		if scopedErr != nil {
			if !utils.IsNotFound(scopedErr) {
				base_log.Errorw("Could not get users. Detail: %v", scopedErr)
			}
			return scopedErr
		}

		users = resUser

		resOrder, scopedErr := h.OrderManager.GetLastOrder(ctx)
		if scopedErr != nil {
			if !utils.IsNotFound(scopedErr) {
				base_log.Errorw("Could not get lastOrder. Detail: %v", scopedErr)
			}
			return scopedErr
		}
		lastOrder = resOrder
		return nil
	}()
	if err != nil || users == nil || products == nil {
		httpClient.RespondError(w, http.StatusInternalServerError, fmt.Sprintf("%v", err))
		return
	}

	mapProductByName := getMapProducts(&products)
	listFailIndex := make([]int, 0)
	for idx, od := range form.Data {
		product := mapProductByName[od.ProductName]
		if product == nil {
			base_log.Warnw("Product name not equal: ", od.ProductName)
			listFailIndex = append(listFailIndex, idx)
			continue
		}
		next := pkg.GetNextAssignee(users, lastOrder)
		orderPC := pkg.OrderPackageCommon{h.OrderManager, "Imported file"}
		if err = orderPC.CreateOrder(&order_model.OrderDto{Order: od.Order}, next, product); err != nil {
			base_log.Errorw("Could not create order", "order", od, "err", err)
			listFailIndex = append(listFailIndex, idx)
			continue
		}
	}

	httpClient.RespondSuccess(w, http.StatusOK, map[string]interface{}{
		"success":           true,
		"list_failed_index": listFailIndex,
	})
}

func getMapProducts(products *[]*products2.Product) map[string]*products2.Product {
	res := make(map[string]*products2.Product)
	for _, prod := range *products {
		res[prod.Name] = prod
	}
	return res
}
