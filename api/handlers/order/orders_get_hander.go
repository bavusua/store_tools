package order

import (
	"bitbucket.org/bavusua/store_tools/api/models/order"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"github.com/spf13/cast"
	"net/http"
	"strings"
)

type OrdersGetHandler struct {
	OrderManager *order_model.OrderManager
}

func(h *OrdersGetHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	params := httpClient.GetCommonParamsFromRequest(r)
	statuses := getSliceString(r.URL.Query().Get("multi_status"))
	productIdsStr := getSliceString(r.URL.Query().Get("product_id"))// tao luoi` sua? code, ke me no nhe

	productIds := make([]int64, 0)
	for _, val := range productIdsStr {
		i := cast.ToInt64(val)
		if i > 0 {
			productIds = append(productIds, i)
		}
	}

	req:= order_model.OrderRequest{
		ShippingCode:   r.URL.Query().Get("shipping_code"),
		Status:         params.Status,
		Statuses:		statuses,
		Ids:            params.Ids,
		OrderId:        params.Id,
		PhoneNumber:    r.URL.Query().Get("phone_customer"),
		Source:         r.URL.Query().Get("source"),
		LandingPage:    r.URL.Query().Get("landing_page"),
		ProductIds:     productIds,
		From:           params.From,
		To:             params.To,
		UserId:			params.UserId,
	}
	//if params.Role == utils.ROLE_TELESALES {
	//	req.UserId = params.RequestUser
	//}

	orders, err := h.OrderManager.ReadDtos(ctx, &req)
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, orders)
	}
	return
}

func getSliceString(param string) []string {
	resp := make([]string, 0)
	if strings.Trim(param, " ") != "" {
		spl := strings.Split(param, ",")
		if len(spl) > 0 {
			for _, stt := range spl {
				if trimStt := strings.Trim(stt, " "); trimStt != "" {
					resp = append(resp, trimStt)
				}
			}
		}
	}
	return resp
}
