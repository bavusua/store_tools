package order

import (
	"bitbucket.org/bavusua/store_tools/api/models/order"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"encoding/json"
	"fmt"
	"net/http"
)

type OrdersReAssignHandler struct {
	OrderManager  *order_model.OrderManager
}

func (h *OrdersReAssignHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	form := order_model.OrderRequest{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&form)
	if err != nil {
		fmt.Print(err)
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}
	if form.UserId < 1 {
		httpClient.RespondError(w, http.StatusBadRequest, "UserId is required")
		return
	}

	err = h.OrderManager.ReAssign(ctx, &form)
	if err != nil {
		fmt.Printf("Could not update order. Detail: %v", err)
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, form)
	}
	return
}