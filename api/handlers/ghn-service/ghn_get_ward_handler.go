package ghn_service

import (
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	ghnService "bitbucket.org/bavusua/store_tools/api/services/ghn"
	"github.com/spf13/cast"
	"net/http"
)

type GHNGetWardsHandle struct {}

func(h *GHNGetWardsHandle) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	district := cast.ToInt(r.URL.Query().Get("district"))
	if district < 1 {
		httpClient.RespondError(w, http.StatusBadRequest, "Invalid district")
		return
	}

	res, err := ghnService.GetWardByDistrictId(district)
	if err !=nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	httpClient.RespondSuccess(w, http.StatusOK, res)
	return
}