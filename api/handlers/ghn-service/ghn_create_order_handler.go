package ghn_service

import (
	"bitbucket.org/bavusua/store_tools/api/models/order"
	product_model "bitbucket.org/bavusua/store_tools/api/models/product"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	ghnService "bitbucket.org/bavusua/store_tools/api/services/ghn"
	"bitbucket.org/bavusua/store_tools/api/utils"
	"context"
	"encoding/json"
	"fmt"
	"github.com/spf13/cast"
	"net/http"
	"store_tools/pkg"
	log "store_tools/scheduler/pkg/base-log"
	"sync"
)

type GHNCreateOrderHandle struct {
	OrderManager *order_model.OrderManager
	ProductManager *product_model.ProductManager
}

type CreateShipmentRequest struct {
	OrderIds []int64 `json:"order_ids"`
}

func (h *GHNCreateOrderHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	form := CreateShipmentRequest{}
	if err := json.NewDecoder(r.Body).Decode(&form); err != nil || len(form.OrderIds) == 0 {
		fmt.Print(err)
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}

	products, err := h.ProductManager.Read(r.Context(), nil)
	if err != nil {
		if !utils.IsNotFound(err) {
			log.Errorw("Could not get product. Detail: %v", err)
		}
		fmt.Print(err)
		httpClient.RespondError(w, http.StatusInternalServerError, "Error get products data")
		return
	}
	mapProduct := make(map[int64]*product_model.Product)
	for _, p := range products {
		mapProduct[p.Id] = p
	}

	//update status order
	affected, err := h.OrderManager.CreatingShipment(r.Context(), form.OrderIds)
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	//create ghn order
	if affected < 10 {
		h.CreateGHNOrders(form.OrderIds, mapProduct)
	} else {
		go func() {
			fromIndex := 0
			maxIndex := len(form.OrderIds)

			for fromIndex < maxIndex {
				toIndex := fromIndex + 10
				if toIndex > maxIndex {
					toIndex = maxIndex
				}
				ids := form.OrderIds[fromIndex:toIndex]
				h.CreateGHNOrders(ids, mapProduct)
				fromIndex += 10
			}
		}()
	}

	httpClient.RespondSuccess(w, http.StatusOK, map[string]interface{}{
		"affected_rows": affected,
		"message": fmt.Sprintf("Xuất kho %v đơn", affected),
	})
	return
}

func (h *GHNCreateOrderHandle) CreateGHNOrders(ids []int64, mapProduct map[int64]*product_model.Product) {
	ctx := context.Background()
	orders, err := h.OrderManager.ReadOrders(ctx, &order_model.OrderRequest{
		ShippingStatus: utils.SHIPPING_STT_CREATING,
		Status:         utils.STATUS_BUYER_CONFIRMED,
		Ids:            ids,
	})
	if err != nil && !utils.IsNotFound(err) {
		fmt.Println(err)
		return
	}
	var gw sync.WaitGroup
	gw.Add(len(orders))
	for _, o := range orders {
		go func(order *order_model.Order) {
			defer gw.Done()
			if order.ShippingCompany == utils.SHIPPING_COMPANNY_LOCAL {
				order.Status = utils.STATUS_LOCAL_AWAITING_SHIPMENT
				inErr := h.OrderManager.UpdateOrder(ctx, order)
				if inErr != nil {
					fmt.Println(inErr)
				}
				return
			}
			// create order GHN
			if order.GHNRequest.FromDistrictID <= 0 {
				order.GHNRequest = pkg.InitGHNRequest(order.CustomerName, order.PhoneNumber, mapProduct[order.ProductId])
			}
			order.GHNRequest.Note = order.ShippingNote
			order.GHNRequest.ExternalCode = cast.ToString(order.Id)
			order.GHNRequest.CustomerPhone = order.PhoneNumber
			order.GHNRequest.CustomerName = order.CustomerName
			order.GHNRequest.ExternalReturnCode = cast.ToString(order.Id)
			res, inErr := ghnService.CreateOrder(*order.GHNRequest)
			if inErr != nil {
				fmt.Println(inErr)
				fmt.Println(ghnService.ToStringResponseData(res))
				order.ShippingStatus = utils.SHIPPING_STT_FAILED
			} else {
				track := res.Data.(map[string]interface{})
				order.ShippingCode = cast.ToString(track["OrderCode"])
				order.ShippingFee = cast.ToInt64(track["TotalServiceFee"])
				order.ShippingStatus = utils.GHN_STT_ReadyToPick
			}
			inErr = h.OrderManager.UpdateOrder(ctx, order)
			if inErr != nil {
				fmt.Println(inErr)
			}
		}(o)
	}
	gw.Wait()
}
