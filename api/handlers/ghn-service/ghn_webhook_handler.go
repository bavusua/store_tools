package ghn_service

import (
	"bitbucket.org/bavusua/store_tools/api/models/order"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	ghnService "bitbucket.org/bavusua/store_tools/api/services/ghn"
	"encoding/json"
	"github.com/spf13/cast"
	"io/ioutil"
	"net/http"
	"store_tools/api/utils"
	"store_tools/loggers"
)

type GHNWebhookHandle struct {
	OrderManager *order_model.OrderManager
}

func (h *GHNWebhookHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	form := ghnService.GHNOrder{}
	ctx := r.Context()
	body, err := ioutil.ReadAll(r.Body)
	loggers.Infow(string(body))
	err = json.Unmarshal(body, &form)
	orderId := cast.ToInt64(form.ExternalCode)
	if err != nil || orderId == 0 {
		loggers.Errorw("Parse failed", "payload", string(body), "detail", err)
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}
	orders, err := h.OrderManager.ReadOrders(ctx, &order_model.OrderRequest{
		Ids: []int64{orderId},
	})
	if err != nil {
		loggers.Errorw("[Webhook] Get order got error",
			"payload", string(body), "detail", err)
		httpClient.RespondError(w, http.StatusInternalServerError, "Could not parse request")
		return
	}
	order := orders[0]
	order.Status = GetStatus(form.CurrentStatus, order.Status)
	order.ShippingStatus = form.CurrentStatus
	err = h.OrderManager.UpdateOrder(ctx, order)
	if err != nil {
		loggers.Errorw("[Webhook] Update order got error",
			"payload", string(body), "detail", err)
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	httpClient.RespondSuccess(w, http.StatusOK, "Happy day")
	return
}

func GetStatus(shippingStt, stt string) string {
	switch shippingStt {
	case utils.GHN_STT_Storing:
		return utils.STATUS_SHIPPED
	case utils.GHN_STT_Delivered:
		return utils.STATUS_DELIVERED
	case utils.GHN_STT_Return:
		return utils.STATUS_RETURN
	case utils.GHN_STT_Returned:
		return utils.STATUS_DELAYED
	case utils.GHN_STT_WaitingToFinish:
		if stt != utils.STATUS_DELIVERED {
			return utils.STATUS_DELAYED
		}
		return stt
	case utils.GHN_STT_Finish:
		if stt == utils.STATUS_DELAYED {
			return utils.STATUS_RETURNED
		}
		return stt
	case utils.GHN_STT_ReadyToPick, utils.GHN_STT_Picking, utils.GHN_STT_Delivering,
		utils.GHN_STT_Cancel, utils.GHN_STT_LostOrder:
		return stt
	default:
		loggers.Infow("Unknow shipping status: " + shippingStt)
		return stt
	}
}
