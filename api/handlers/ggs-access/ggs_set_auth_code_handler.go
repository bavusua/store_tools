package ggs_access

import (
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	base_log "bitbucket.org/bavusua/store_tools/scheduler/pkg/base-log"
	"encoding/json"
	"net/http"
)

type SetAuthCodeRequest struct {
	AuthCode string `json:"auth_code"`
}

type GgsSetAuthCodeHandler struct {

}

func(h *GgsSetAuthCodeHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	value := SetAuthCodeRequest{}

	if err := json.NewDecoder(r.Body).Decode(&value); err != nil {
		base_log.Errorw("error set auth code", err)
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}
	httpClient.SetAuthCode(value.AuthCode)
	base_log.Infow("set auth_code: ", value)
	httpClient.RespondSuccess(w, http.StatusOK, nil)
}



