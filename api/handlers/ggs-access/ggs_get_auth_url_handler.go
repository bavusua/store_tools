package ggs_access

import (
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	base_log "bitbucket.org/bavusua/store_tools/scheduler/pkg/base-log"
	"net/http"
)

type GgsGetAuthUrlHandler struct {

}

func(h *GgsGetAuthUrlHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	base_log.Infow("get authUrl: ", httpClient.GetAuthUrl())
	httpClient.RespondSuccess(w, http.StatusOK, map[string]interface{}{
		"auth_url": httpClient.GetAuthUrl(),
	})
}
