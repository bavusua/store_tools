package ggs_access

import (
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	base_log "bitbucket.org/bavusua/store_tools/scheduler/pkg/base-log"
	"encoding/json"
	"net/http"
)

type AuthUrlRequest struct {
	AuthURL string `json:"auth_url"`
}
type GgsSetAuthUrlHandler struct {

}

func(h *GgsSetAuthUrlHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	value := AuthUrlRequest{}

	if err := json.NewDecoder(r.Body).Decode(&value); err != nil {
		base_log.Errorw("error set auth url", err)
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}
	httpClient.SetAuthURL(value.AuthURL)
	base_log.Infow("set auth url: ", value.AuthURL)
	httpClient.RespondSuccess(w, http.StatusOK, nil)
}

