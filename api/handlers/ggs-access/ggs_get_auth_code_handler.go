package ggs_access

import (
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	base_log "bitbucket.org/bavusua/store_tools/scheduler/pkg/base-log"
	"net/http"
	global_cache "bitbucket.org/bavusua/store_tools/api/pkg"
)

type GgsGetAuthCodeHandler struct {

}

func(h *GgsGetAuthCodeHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	base_log.Infow("get auth code: ", global_cache.GetAuthCode())
	httpClient.RespondSuccess(w, http.StatusOK, map[string]interface{}{
		"auth_code": global_cache.GetAuthCode(),
	})
}
