package analytics

import (
	"github.com/spf13/cast"
	"net/http"
	"store_tools/api/models"
	httpClient "store_tools/api/pkg"
	"time"
)

type GetProductAnalyticsHandler struct {
	Analytics *models.Analytics
}

type GetTelesalesAnalyticsHandler struct {
	Analytics *models.Analytics
}

func(h *GetProductAnalyticsHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	yesterday := cast.ToInt64(r.URL.Query().Get("yesterday"))
	today := cast.ToInt64(r.URL.Query().Get("today"))
	rs, err := h.Analytics.GetProductAnalytics(
		r.Context(),
		time.Unix(yesterday, 0),
		time.Unix(today, 0))
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, rs)
	}
	return
}


func(h *GetTelesalesAnalyticsHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	from := cast.ToInt64(r.URL.Query().Get("from"))
	rs, err := h.Analytics.GetTelesalesAnalytics(
		r.Context(),
		time.Unix(from, 0))
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, rs)
	}
	return
}
