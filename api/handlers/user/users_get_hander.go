package user

import (
	"bitbucket.org/bavusua/store_tools/api/models/user"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"encoding/json"
	"github.com/spf13/cast"
	"net/http"
)

type UsersGetHandler struct {
	UserManager *user_model.UserManager
}

func (h *UsersGetHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	req := &user_model.UserRequest{
		Id:         cast.ToInt64(r.URL.Query().Get("id")),
		Role:       r.URL.Query().Get("role"),
		ActiveOnly: cast.ToBool(r.URL.Query().Get("is_active")),
	}
	isGetAll := isGetAllUsers(r)
	if isGetAll {
		if hasUsersFromCache(&w) {
			return
		}
	}

	users, err := h.UserManager.Read(ctx, req)
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		go setUsersToCache(users)
		httpClient.RespondSuccess(w, http.StatusOK, users)
	}
	return
}

func isGetAllUsers(r *http.Request) bool {
	return r.URL.Query().Encode() == ""
}

func hasUsersFromCache(w *http.ResponseWriter) bool {
	val := httpClient.GetCacheValue(httpClient.AllUserCacheData.Key)
	if val != "" {
		res := make([]*user_model.User, 0)
		err := json.Unmarshal([]byte(val), &res)
		if err == nil {
			httpClient.RespondSuccess(*w, http.StatusOK, res)
			return true
		}
	}
	return false
}

func setUsersToCache(users []*user_model.User) {
	val, err := json.Marshal(users)
	cacheKey := httpClient.AllUserCacheData
	if err == nil {
		httpClient.SetCacheValue(
			cacheKey.Key,
			&httpClient.CacheKeyDefinition{
				Key: cacheKey.Key,
				Expiry: cacheKey.Expiry,
				Value: string(val),
			},
		)
	}
}
