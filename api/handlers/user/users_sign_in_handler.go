package user

import (
	"bitbucket.org/bavusua/store_tools/api/models/user"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"bitbucket.org/bavusua/store_tools/api/utils"
	"encoding/json"
	"fmt"
	"net/http"
)

type UsersSignInHandler struct {
	UserManager *user_model.UserManager
}

func (h *UsersSignInHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	form := user_model.User{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&form)
	if err != nil {
		fmt.Print(err.Error())
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}

	user, e := h.UserManager.GetSignIn(ctx, &form)
	if e != nil {
		if utils.IsNotFound(e) {
			httpClient.RespondError(w, http.StatusUnauthorized, "Tài khoản đăng nhập không chính xác")
		}
		httpClient.RespondError(w, http.StatusInternalServerError, e.Error())
	} else {
		token := httpClient.SetLoggedUser(user.Id, user.Username, user.Role)
		httpClient.RespondSuccess(w, http.StatusOK, map[string]string{
			"token": token,
			"role": user.Role,
		})
	}
	return
}
