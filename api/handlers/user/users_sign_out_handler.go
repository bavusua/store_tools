package user

import (
	"bitbucket.org/bavusua/store_tools/api/models/user"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"github.com/spf13/cast"
	"net/http"
)

type UsersSignOutHandler struct {
	UserManger *user_model.UserManager
}

func (h *UsersSignOutHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	user, err := h.UserManger.Read(r.Context(), &user_model.UserRequest{
		Id: cast.ToInt64(r.URL.Query().Get("id")),
	})
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	if httpClient.DeleteLoggedUser(user[0].Username) {
		httpClient.RespondSuccess(w, http.StatusOK, "Logged out")
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, "Not found user")
	}
	return
}
