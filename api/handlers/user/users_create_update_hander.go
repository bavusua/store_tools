package user

import (
	"bitbucket.org/bavusua/store_tools/api/models/user"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"encoding/json"
	"fmt"
	"net/http"
)

type UsersCreateUpdateHandler struct {
	UserManager *user_model.UserManager
}

func(h *UsersCreateUpdateHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	form := user_model.User{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&form)
	if err != nil {
		fmt.Print(err.Error())
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}

	var user *user_model.User
	if form.Id > 0 {
		user, err = h.UserManager.Update(ctx, &form)
	} else {
		user, err = h.UserManager.Create(ctx, &form)
	}
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	}

	go httpClient.DeleteCache(httpClient.AllUserCacheData.Key)
	httpClient.RespondSuccess(w, http.StatusOK, user)
	return
}
