package product

import (
	"bitbucket.org/bavusua/store_tools/api/models/product"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"encoding/json"
	"fmt"
	"net/http"
)

type ProductsCreateUpdateHandler struct {
	ProductManager *product_model.ProductManager
}

func(h *ProductsCreateUpdateHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	form := product_model.Product{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&form)
	if err != nil {
		fmt.Print(err.Error())
		httpClient.RespondError(w, http.StatusBadRequest, "Could not parse request")
		return
	}

	var product *product_model.Product
	if form.Id > 0 {
		product, err = h.ProductManager.Update(ctx, &form)
	} else {
		product, err = h.ProductManager.Create(ctx, &form)
	}
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	}

	go httpClient.DeleteCache(httpClient.AllProductCacheData.Key)
	httpClient.RespondSuccess(w, http.StatusOK, product)
	return
}
