package product

import (
	"net/http"
	"bitbucket.org/bavusua/store_tools/api/pkg"
)

type ClearCacheHandler struct {

}


func (h *ClearCacheHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	pkg.DeleteCache(pkg.AllProductCacheData.Key)
	pkg.RespondSuccess(w, http.StatusOK, nil)
}
