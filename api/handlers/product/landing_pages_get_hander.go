package product

import (
	"bitbucket.org/bavusua/store_tools/api/models/landing-page"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"net/http"
	"store_tools/api/utils"
)

type LandingPagesGetHandler struct {
	LandingPageManager *landing_page_model.LandingPageManager
}

func(h *LandingPagesGetHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	req := httpClient.GetCommonParamsFromRequest(r)
	if req.ProductId < 1 {
		httpClient.RespondError(w, http.StatusBadRequest, "Product id is required")
		return
	}
	if req.Role == utils.ROLE_MARKETING {
		req.UserId = req.RequestUser
	}

	lps, err := h.LandingPageManager.Read(ctx, &req)
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, lps)
	}
	return
}