package product

import (
	"bitbucket.org/bavusua/store_tools/api/models/product"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"encoding/json"
	"net/http"
)

type ProductsGetHandler struct {
	ProductManager *product_model.ProductManager
}

func (h *ProductsGetHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	req := httpClient.GetCommonParamsFromRequest(r)
	isGetAll := isGetAllProduct(r)
	if isGetAll {
		if hasProductsFromCache(&w) {
			return
		}
	}

	products, err := h.ProductManager.Read(ctx, &req)
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		go setProductsToCache(products)
		httpClient.RespondSuccess(w, http.StatusOK, products)
	}
	return
}

func isGetAllProduct(r *http.Request) bool {
	return r.URL.Query().Encode() == ""
}

func hasProductsFromCache(w *http.ResponseWriter) bool {
	val := httpClient.GetCacheValue(httpClient.AllProductCacheData.Key)
	if val != "" {
		res := make([]*product_model.Product, 0)
		err := json.Unmarshal([]byte(val), &res)
		if err == nil {
			httpClient.RespondSuccess(*w, http.StatusOK, res)
			return true
		}
	}
	return false
}

func setProductsToCache(product []*product_model.Product) {
	val, err := json.Marshal(product)
	cacheKey := httpClient.AllProductCacheData
	if err == nil {
		httpClient.SetCacheValue(
			cacheKey.Key,
			&httpClient.CacheKeyDefinition{
				Key: cacheKey.Key,
				Expiry: cacheKey.Expiry,
				Value: string(val),
			},
		)
	}
}
