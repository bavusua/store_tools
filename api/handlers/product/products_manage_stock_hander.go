package product

import (
	"bitbucket.org/bavusua/store_tools/api/models/product"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"encoding/json"
	"net/http"
	"store_tools/api/pkg"
	"store_tools/api/utils"
)

type ProductsManageStockHandler struct {
	ProductManager *product_model.ProductManager
}

func(h *ProductsManageStockHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	form := product_model.Product{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&form)
	if err != nil {
		httpClient.RespondError(w, http.StatusBadRequest, err.Error())
		return
	}
	params := pkg.GetCommonParamsFromRequest(r)
	var selectFields []string
	switch params.Role {
	case utils.ROLE_MARKETING:
		selectFields = []string{"budget"}
	case utils.ROLE_PTSP:
		selectFields = []string{"requesting_stock", "incoming_stock"}
	case utils.ROLE_ADMIN:
		selectFields = []string{"requesting_stock", "incoming_stock", "budget"}
	default:
		httpClient.RespondError(w, http.StatusUnauthorized, params.Role)
		return
	}

	_, err = h.ProductManager.ManageStock(ctx, &form, selectFields)
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, "ok")
	}
	return
}