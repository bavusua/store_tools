package product

import (
	"bitbucket.org/bavusua/store_tools/api/models/landing-page"
	httpClient "bitbucket.org/bavusua/store_tools/api/pkg"
	"encoding/json"
	"net/http"
	"store_tools/api/pkg"
)

type LandingPagesCreateHandler struct {
	LandingPageManager *landing_page_model.LandingPageManager
}

func(h *LandingPagesCreateHandler) ServeHTTP (w http.ResponseWriter, r *http.Request) {
	form := landing_page_model.LandingPage{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&form)
	if err != nil {
		httpClient.RespondError(w, http.StatusBadRequest, err.Error())
		return
	}
	params := pkg.GetCommonParamsFromRequest(r)
	form.UserId = params.RequestUser

	var ld *landing_page_model.LandingPage
	ld, err = h.LandingPageManager.Create(ctx, &form)
	if err != nil {
		httpClient.RespondError(w, http.StatusInternalServerError, err.Error())
	} else {
		httpClient.RespondSuccess(w, http.StatusOK, ld)
	}
	return
}